/*
 * @Author: your name
 * @Date: 2021-07-26 14:34:21
 * @LastEditTime: 2021-07-28 15:01:35
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\apis\card.dart
 */
import 'package:flutter_faster_study/conf/api.dart';
import 'package:flutter_faster_study/utils/request.dart';

// 获取浏览总量
getViewCount() async {
  return NetRequest().reqeustData(H5Api.VIEW_COUNT).then((res) {
    if (res.code == 200) {
      return res.data;
    }
  }).catchError((error) {
    print(error);
  });
}

// 获取预约总量
getApplyCount() async {
  return NetRequest().reqeustData(H5Api.APPLY_COUNT).then((res) {
    print(res);
    if (res.code == 200) {
      return res.data;
    }
  }).catchError((error) {
    print("拨错啦 apply");
    print(error);
  });
}
