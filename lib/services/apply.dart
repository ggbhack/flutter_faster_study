/*
 * @Author: your name
 * @Date: 2021-07-26 14:34:42
 * @LastEditTime: 2021-07-29 14:52:20
 * @LastEditors: GGB
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\services\apply.dart
 */

import 'package:flutter_faster_study/conf/api.dart';
import 'package:flutter_faster_study/model/apply.dart';
import 'package:flutter_faster_study/utils/request.dart';

// 获取产品数据
getApply(String type) async {
  return NetRequest().reqeustData("${H5Api.APPLY_PAGE}$type").then((res) {
    if (res.code == 200) {
      return res.data;
    }
  }).catchError((error) {
    print("报错了");
  });
}

updateGiftStatus(ApplyModel apply) async {
  return NetRequest()
      .reqeustData("${H5Api.UPDATE_APPLY}${apply.id}")
      .then((res) {
    if (res.code == 200) {
      return res.data;
    }
  }).catchError((error) {
    print(error);
  });
}
