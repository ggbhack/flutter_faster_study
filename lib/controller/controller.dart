/*
 * @Author: your name
 * @Date: 2021-07-26 15:30:11
 * @LastEditTime: 2021-07-29 14:50:49
 * @LastEditors: GGB
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\controller\controller.dart
 */
import 'package:flutter_faster_study/model/apply.dart';
import 'package:flutter_faster_study/model/menu.dart';
import 'package:flutter_faster_study/services/apply.dart';
import 'package:flutter_faster_study/services/card.dart';
import 'package:get/get.dart';

class ApplyController extends GetxController {
  // 获取预约列表数据
  List<ApplyModel> _applyList = [];
  List get applyList => _applyList;
  void requestApply() async {
    _applyList = [];
    var data = await getApply(this.type);
    for (var item in data) {
      _applyList.add(ApplyModel.fromJson((item)));
    }
    update();
  }

  // 获取浏览量数据统计
  int _viewCount = 0;
  int get viewCount => _viewCount;
  void requestViewCount() async {
    var viewData = await getViewCount();
    _viewCount = viewData['allNum'];
    update();
  }

// 获取预约数据统计
  int _applyCount = 0;
  int get applyCount => _applyCount;

  void requestApplyCount() async {
    var viewData = await getApplyCount();
    _applyCount = viewData['allNum'];
    update();
  }

  // 选择的类型
  String _type = "";
  String get type => _type;
  // 设置赛选类型
  void setType(String value) {
    _type = value;
    update();
    this.requestApply();
  }

  // 菜单选项
  List<MenuModel> _menus = [
    MenuModel("全部", ""),
    MenuModel("已领取", "1"),
    MenuModel("未领取", "0"),
  ];

  List<MenuModel> get menus => _menus;

  String getSelected() {
    String type = "所有";
    for (var item in _menus) {
      if (item.value == this.type) type = item.label;
    }
    return type;
  }

  // 发送礼物
  void sendGift(ApplyModel apply) async {
    print(apply.name);
  }
}
