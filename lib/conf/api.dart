/*
 * @Author: your name
 * @Date: 2021-07-26 16:54:42
 * @LastEditTime: 2021-07-29 15:11:04
 * @LastEditors: GGB
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\conf\api.dart
 */

class H5Api {
  static const String BASE_URL = "https://xxxxxx";
  // 返回首页请求的json
  static const String APPLY_PAGE =
      BASE_URL + "/orderUser/getAllOrByIsReceive?isReceive=";
  // 分类页的导航
  static const String VIEW_COUNT = BASE_URL + '/wxUser/countAll';
  // 分类页的商品类目的json数据
  static const String APPLY_COUNT = BASE_URL + '/orderUser/countAll';
  // 更改礼物
  static const String UPDATE_APPLY =
      BASE_URL + '/orderUser/updateIsReceiveById?id=';
}
