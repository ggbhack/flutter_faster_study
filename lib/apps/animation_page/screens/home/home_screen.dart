/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 21:17:06
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 21:54:38
 */
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/animation_page/screens/details/first_screen.dart';
import 'package:flutter_faster_study/apps/animation_page/screens/details/second_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isFirst = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("动画效果仿页面切换"),
      ),
      body: PageTransitionSwitcher(
        duration: Duration(milliseconds: 1200),
        reverse: isFirst, // 更好的体验优化
        transitionBuilder: (child, animation, secondaryAnimation) =>
            SharedAxisTransition(
          child: child,
          animation: animation,
          secondaryAnimation: secondaryAnimation,
          transitionType:
              SharedAxisTransitionType.horizontal, // 动画类型，水平方向过度下一个页面
        ),
        child: isFirst ? FirstScreen() : SecondScreen(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.menu),
        onPressed: () {
          setState(() {
            isFirst = !isFirst;
          });
        },
      ),
    );
  }
}
