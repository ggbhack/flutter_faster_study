/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 21:33:05
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 21:55:03
 */
import 'package:flutter/material.dart';

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Image.asset("assets/images/bg.jpg"),
    );
  }
}
