/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 21:34:33
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 21:58:10
 */
import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Image.asset("assets/images/bg1.jpg", fit: BoxFit.fill),
    );
  }
}
