/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 17:30:40
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 18:26:57
 */
import 'dart:ui';

import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  double blurImage = 0.0;
  double blurImage1 = 0.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("模糊小部件"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: ListView(
          children: [
            buildBlurredImage(),
            const SizedBox(height: 20),
            Slider(
                value: blurImage,
                onChanged: (value) => setState(() {
                      blurImage = value;
                    })),
            const SizedBox(height: 20),
            buildImageOverlay(),
            Slider(
                value: blurImage1,
                onChanged: (value) => setState(() {
                      blurImage1 = value;
                    })),
          ],
        ),
      ),
    );
  }

  Widget buildBlurredImage() => ClipRRect(
        borderRadius: BorderRadius.circular(24),
        child: Stack(
          children: [
            Image.network(
                'https://tse1-mm.cn.bing.net/th/id/R-C.3140c20aadff937e10f24f5d90b1ce54?rik=0tAXkgIzPJxkaQ&riu=http%3a%2f%2fwww.uuudoc.com%2fimg%2f2W3838341M1B1B2X312P2V2T1D1C1L1A1F1I1C2S332R1A2R33311B1W333B3230332P2S21312V1B1E1C1D1L1B1C1G1B1D1F1D1J1B1D1H1K1J1C1K1E1L1L2N1D1E2N1E1C1D1L1C1G1D1F1C1H1D1E1D1J1K1D1H.jpg&ehk=QjeFDaRDWx5NS%2bQ959PAW4RFB7UCzOgR6UnJH5w9s2s%3d&risl=&pid=ImgRaw&r=0',
                fit: BoxFit.fill),
            Positioned.fill(
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: blurImage * 100,
                  sigmaY: blurImage * 100,
                ),
                child: Container(
                  color: Colors.black.withOpacity(0.2),
                ),
              ),
            ),
          ],
        ),
      );

  buildImageOverlay() => ClipRRect(
        borderRadius: BorderRadius.circular(24),
        child: Stack(
          children: [
            Image.network(
                'https://tse1-mm.cn.bing.net/th/id/R-C.9190f03528ea1a77d6b5da2cbc7e56eb?rik=x5wngLUlX48tIg&riu=http%3a%2f%2fwww.shijuepi.com%2fuploads%2fallimg%2f200924%2f1-200924110P5.jpg&ehk=gNZ01QBvm%2bjzaj0i3Dp3HHf%2bXtWueP2Fa3kAMp%2f1aGw%3d&risl=&pid=ImgRaw&r=0',
                fit: BoxFit.fill),
            // Positioned.fill(
            //   child: BackdropFilter(
            //     filter: ImageFilter.blur(
            //       sigmaX: 10,
            //       sigmaY: 10,
            //     ),
            //     child: Container(
            //       color: Colors.black.withOpacity(0.2),
            //     ),
            //   ),
            // ),
            Positioned(
              top: 12,
              left: 0,
              right: 0,
              child: Center(
                  child: buildBlur(
                borderRadius: BorderRadius.circular(20),
                child: Container(
                  padding: EdgeInsets.all(24),
                  color: Colors.white.withOpacity(0.4),
                  child: Text(
                    "模糊生辰器",
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              )),
            ),
          ],
        ),
      );

  buildBlur({BorderRadius borderRadius, Container child}) => ClipRRect(
        borderRadius: borderRadius ?? BorderRadius.zero,
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: blurImage1 * 100,
            sigmaY: blurImage1 * 100,
          ),
          child: child,
        ),
      );
}
