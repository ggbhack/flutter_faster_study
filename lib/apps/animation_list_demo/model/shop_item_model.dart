class ShopItemModel {
  final String title;
  final String urlImage;
  ShopItemModel({this.title, this.urlImage});
}
