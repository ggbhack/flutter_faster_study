import 'package:flutter_faster_study/apps/animation_list_demo/model/shop_item_model.dart';

class Data {
  static final List<ShopItemModel> shoppingList = [
    ShopItemModel(title: '牛奶', urlImage: 'assets/images/icon.png'),
    ShopItemModel(title: '咖啡', urlImage: 'assets/images/icon.png'),
    ShopItemModel(title: '面包', urlImage: 'assets/images/icon.png'),
    ShopItemModel(title: '土豆', urlImage: 'assets/images/icon.png'),
    ShopItemModel(title: '油条', urlImage: 'assets/images/icon.png'),
    ShopItemModel(title: '点心', urlImage: 'assets/images/icon.png'),
    ShopItemModel(title: '蛋糕', urlImage: 'assets/images/icon.png'),
  ];
}
