import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/animation_list_demo/model/shop_item_model.dart';

class ShopItemWidget extends StatelessWidget {
  final ShopItemModel item;
  final Animation animation;
  final VoidCallback onClicked;
  const ShopItemWidget(
      {@required this.item,
      @required this.animation,
      @required this.onClicked,
      Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) => ScaleTransition(
        scale: animation,
        child: Container(
          margin: EdgeInsets.all(8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
          ),
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
            leading: CircleAvatar(
              radius: 32,
              backgroundImage: AssetImage(item.urlImage),
            ),
            title: Text(
              item.title,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            trailing: IconButton(
              icon: Icon(Icons.check_circle, color: Colors.green, size: 32),
              onPressed: onClicked,
            ),
          ),
        ),
      );
}
