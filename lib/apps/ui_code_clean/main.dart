import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/screens/home/home_screen.dart';

// import 'screens/home_01/home_screen.dart'; //第二个玻璃屏效果

// void main() {
//   runApp(MyApp());
// }

Future main() async {
  // 初始化
  WidgetsFlutterBinding.ensureInitialized();
  // 设置横竖屏 锁定方向
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'onBoading',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.transparent,
        ),
      ),
      home: HomeScreen(),
    );
  }
}
