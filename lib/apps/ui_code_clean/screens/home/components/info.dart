/*
 * @Author: your name
 * @Date: 2021-07-26 07:32:31
 * @LastEditTime: 2021-07-28 09:38:56
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\apps\ui_code_clean\screens\home\components\info.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';

class Info extends StatelessWidget {
  const Info({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(child: BuildInfoItem(label: "身高", value: 180)),
          Expanded(child: BuildInfoItem(label: "体重", value: 70)),
          Expanded(child: BuildInfoItem(label: "大卡", value: 280)),
        ],
      ),
    );
  }
}

class BuildInfoItem extends StatelessWidget {
  const BuildInfoItem({Key key, this.label, this.value}) : super(key: key);
  final String label;
  final int value;

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.all(5),
        child: Center(
          child: GlassWidget(
            width: double.infinity,
            height: 50,
            borderRadius: 5,
            blur: 10,
            color: Color(0xffffff),
            linearGradientBeginOpacity: 0.4,
            linearGradientEndOpacity: 0.4,
            borderGradientBeginOpacity: 0.5,
            borderGradientEndOpacity: 0.1,
            child: Center(
              child: RichText(
                text: TextSpan(
                  text: label,
                  style: TextStyle(
                    color: Color(0xff757575),
                    fontSize: 16,
                    fontWeight: FontWeight.normal,
                  ),
                  children: [
                    TextSpan(
                      text: "$value",
                      style: TextStyle(
                        color: Color(0xFFe82c5c),
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      );
}
