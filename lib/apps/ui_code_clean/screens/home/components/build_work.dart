/*
 * @Author: your name
 * @Date: 2021-07-27 21:07:39
 * @LastEditTime: 2021-07-28 09:45:01
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\apps\ui_code_clean\screens\home\components\build_work.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/components/glass_widget.dart';

class BuildWork extends StatelessWidget {
  const BuildWork({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.only(top: 7),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: BuildWorkItem(
                  icon: "assets/images/workout.png", title: "健身日程"),
            ),
            Expanded(
              child:
                  BuildWorkItem(icon: "assets/images/diet.png", title: "健康饮食"),
            ),
          ],
        ),
      );
}

class BuildWorkItem extends StatelessWidget {
  const BuildWorkItem({Key key, this.icon, this.title}) : super(key: key);
  final String icon;
  final String title;

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.all(5),
        child: Center(
          child: GlassWidget(
            width: double.infinity,
            height: 60,
            borderRadius: 10,
            blur: 10,
            linearGradientBeginOpacity: 0.4,
            linearGradientEndOpacity: 0.4,
            borderGradientBeginOpacity: 0.5,
            borderGradientEndOpacity: 0.1,
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    icon,
                    height: 40,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    title,
                    style: TextStyle(
                      color: Color(0xff535353),
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
