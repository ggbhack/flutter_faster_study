/*
 * @Author: your name
 * @Date: 2021-07-26 07:48:51
 * @LastEditTime: 2021-07-28 09:42:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\apps\ui_code_clean\screens\home\components\build_message.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';

class BuildMessage extends StatelessWidget {
  const BuildMessage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 40),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child:
                BuildMessageItem(icon: "assets/images/heart.png", title: "心跳"),
          ),
          Expanded(
            child:
                BuildMessageItem(icon: "assets/images/water.png", title: "补水"),
          ),
          Expanded(
            child:
                BuildMessageItem(icon: "assets/images/moon.png", title: "睡眠"),
          ),
        ],
      ),
    );
  }
}

class BuildMessageItem extends StatelessWidget {
  const BuildMessageItem({Key key, this.icon, this.title}) : super(key: key);
  final String icon;
  final String title;

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.all(5),
        child: Center(
          child: GlassWidget(
              width: double.infinity,
              height: 110,
              borderRadius: 10,
              blur: 10,
              color: Color(0xffffff),
              linearGradientBeginOpacity: 0.4,
              linearGradientEndOpacity: 0.4,
              borderGradientBeginOpacity: 0.5,
              borderGradientEndOpacity: 0.1,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      icon,
                      height: 40,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      title,
                      style: TextStyle(
                          color: Color(0xff535353),
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              )),
        ),
      );
}
