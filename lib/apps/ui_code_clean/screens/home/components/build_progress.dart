import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class BuildProgress extends StatelessWidget {
  const BuildProgress({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SleekCircularSlider(
      innerWidget: (double value) {
        return Center(
          child: buildCenterInfo(),
        );
      },
      appearance: CircularSliderAppearance(
        startAngle: -80,
        angleRange: 360,
        size: 250,
        customColors: CustomSliderColors(
          trackColor: Color(0xffffffff).withOpacity(0.6),
          progressBarColors: [
            Color(0xfffec753),
            Color(0xfff16d58),
          ],
          shadowColor: Color(0xffffffff).withOpacity(0.1),
        ),
        customWidths: CustomSliderWidths(
          progressBarWidth: 30,
          trackWidth: 30,
        ),
      ),
    );
  }

  Widget buildCenterInfo() => GlassWidget(
        width: 130,
        height: 130,
        borderRadius: 65,
        blur: 10,
        color: Color(0xffffff),
        linearGradientBeginOpacity: 0.6,
        linearGradientEndOpacity: 0.4,
        borderGradientBeginOpacity: 0.2,
        borderGradientEndOpacity: 0.05,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '6550',
                style: TextStyle(
                  color: Color(0xfff26a5a).withOpacity(0.9),
                  fontSize: 30,
                  letterSpacing: -2,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text(
                '步数',
                style: TextStyle(
                  color: Color(0xfff79056).withOpacity(0.9),
                  fontSize: 15,
                  letterSpacing: -2,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        ),
      );
}
