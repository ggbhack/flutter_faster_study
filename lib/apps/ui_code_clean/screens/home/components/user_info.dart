/*
 * @Author: your name
 * @Date: 2021-07-26 07:27:26
 * @LastEditTime: 2021-07-28 09:31:37
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\apps\ui_code_clean\screens\home\components\user_info.dart
 */
import 'package:flutter/material.dart';

class UserInfo extends StatelessWidget {
  const UserInfo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        buildName(),
        buildPic(),
      ],
    );
  }

  Widget buildName() => Container(
        padding: EdgeInsets.only(left: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '星期五',
              style: TextStyle(
                color: Color(0xFFeb3c63),
                fontSize: 30,
                letterSpacing: 1,
                fontWeight: FontWeight.w700,
              ),
            ),
            Text(
              'GGB',
              style: TextStyle(
                color: Color(0xFFef7079),
                fontSize: 20,
                letterSpacing: 1,
                fontWeight: FontWeight.w500,
              ),
            )
          ],
        ),
      );

  Widget buildPic() => Padding(
        padding: const EdgeInsets.only(right: 15),
        child: Container(
          padding: EdgeInsets.all(5),
          height: 100,
          width: 100,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color(0xFFec445b).withOpacity(0.8),
                Color(0xFFfbb055).withOpacity(0.8)
              ],
            ),
          ),
          child: Container(
            width: 90,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('assets/images/avatar.jpg'),
              ),
            ),
          ),
        ),
      );
}
