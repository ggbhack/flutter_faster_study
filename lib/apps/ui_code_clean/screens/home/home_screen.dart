/*
 * @Author: your name
 * @Date: 2021-07-26 07:04:25
 * @LastEditTime: 2021-07-28 09:46:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\apps\ui_code_clean\screens\home\home_screen.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';

import '../../components/build_bottom.dart';
import 'components/build_message.dart';
import 'components/build_progress.dart';
import 'components/build_work.dart';
import 'components/info.dart';
import 'components/user_info.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      bottomNavigationBar: BuildBottom(
        selected: 0,
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Stack(
          children: [
            Image.asset(
              'assets/images/bg.jpg',
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
              scale: 1,
            ),
            SafeArea(
              child: Center(
                child: GlassWidget(
                  width: width * 0.9,
                  height: height * 0.85,
                  borderRadius: 30,
                  blur: 20,
                  color: Color(0xffffff),
                  linearGradientBeginOpacity: 0.3,
                  linearGradientEndOpacity: 0.2,
                  borderGradientBeginOpacity: 0.5,
                  borderGradientEndOpacity: 0.02,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 30,
                          ),
                          UserInfo(),
                          Info(),
                          SizedBox(
                            height: 30,
                          ),
                          BuildProgress(),
                          BuildMessage(),
                          BuildWork(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildWork() => Container(
        padding: EdgeInsets.only(top: 7),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            buildWorkItem('assets/workout.png', '健身日程'),
            buildWorkItem('assets/diet.png', '健康饮食')
          ],
        ),
      );
  Widget buildWorkItem(String image, String text) => Center(
        child: GlassWidget(
          width: 168,
          height: 60,
          borderRadius: 10,
          blur: 10,
          linearGradientBeginOpacity: 0.4,
          linearGradientEndOpacity: 0.4,
          borderGradientBeginOpacity: 0.5,
          borderGradientEndOpacity: 0.1,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  image,
                  height: 40,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  text,
                  style: TextStyle(
                      color: Color(0xff535353),
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ),
      );
  Widget buildBottom() => Container(
        margin: EdgeInsets.only(top: 40),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
              icon: Icon(
                Icons.calendar_today,
                color: Color(0xffe82c5c),
              ),
              onPressed: null,
            ),
            IconButton(
              icon: Icon(Icons.person_outline_outlined),
              onPressed: null,
            ),
            IconButton(
              icon: Icon(Icons.settings_outlined),
              onPressed: null,
            ),
            IconButton(
              icon: Icon(Icons.file_present),
              onPressed: null,
            )
          ],
        ),
      );
}
