/*
 * @Author: your name
 * @Date: 2021-07-27 21:27:38
 * @LastEditTime: 2021-07-28 09:46:24
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\apps\ui_code_clean\screens\home_01\home_screen.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/build_bottom.dart';

import 'components/build_date.dart';
import 'components/build_lines.dart';
import 'components/build_water.dart';

class HomeScreen1 extends StatefulWidget {
  const HomeScreen1({Key key}) : super(key: key);

  @override
  _HomeScreen1State createState() => _HomeScreen1State();
}

class _HomeScreen1State extends State<HomeScreen1> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Stack(
          children: [
            Image.asset(
              'assets/images/bg.jpg',
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
              scale: 1,
            ),
            SafeArea(
              child: Center(
                child: GlassWidget(
                  width: width * 0.9,
                  height: height * 0.85,
                  borderRadius: 30,
                  color: Color(0xffffffff),
                  blur: 20,
                  linearGradientBeginOpacity: 0.3,
                  linearGradientEndOpacity: 0.2,
                  borderGradientBeginOpacity: 0.5,
                  borderGradientEndOpacity: 0.02,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          BuildDate(),
                          SizedBox(height: 30),
                          BuildLines(),
                          SizedBox(height: 30),
                          BuildWater(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BuildBottom(
        selected: 1,
      ),
    );
  }
}
