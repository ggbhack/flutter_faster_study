import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class BuildProgress extends StatelessWidget {
  const BuildProgress({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => SleekCircularSlider(
        innerWidget: (double value) {
          return Center(
            child: buildCenterInfo(),
          );
        },
        appearance: CircularSliderAppearance(
          startAngle: -80,
          angleRange: 480,
          size: 150,
          customColors: CustomSliderColors(
            trackColor: Color(0xffffffff).withOpacity(0.6),
            progressBarColors: [
              Color(0xff5BC8CF),
              Color(0xff5BC8CF),
            ],
            shadowColor: Color(0xffffffff).withOpacity(0.1),
          ),
          customWidths: CustomSliderWidths(
            progressBarWidth: 20,
            trackWidth: 20,
          ),
        ),
      );

  buildCenterInfo() => GlassWidget(
        width: 90,
        height: 90,
        borderRadius: 45,
        color: Color(0xffffffff),
        blur: 5,
        linearGradientBeginOpacity: 0.3,
        linearGradientEndOpacity: 0.2,
        borderGradientBeginOpacity: 0.5,
        borderGradientEndOpacity: 0.02,
        child: Center(
          child: Text(
            "1000ml",
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      );
}
