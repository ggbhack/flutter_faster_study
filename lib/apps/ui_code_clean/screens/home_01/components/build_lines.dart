import 'package:bezier_chart/bezier_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';

class BuildLines extends StatelessWidget {
  const BuildLines({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Center(
        child: GlassWidget(
          width: double.infinity,
          height: 190,
          borderRadius: 20,
          color: Color(0xffffffff),
          blur: 5,
          linearGradientBeginOpacity: 0.3,
          linearGradientEndOpacity: 0.2,
          borderGradientBeginOpacity: 0.5,
          borderGradientEndOpacity: 0.02,
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.all(20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      "assets/images/heart.png",
                      height: 25,
                    ),
                    SizedBox(width: 8),
                    Text(
                      "心跳",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                height: 160,
                width: MediaQuery.of(context).size.width * 0.9,
                child: BezierChart(
                  footerValueBuilder: (double value) {
                    int intValue = value.toInt();
                    switch (intValue) {
                      case 0:
                        return '6:00';
                        break;
                      case 1:
                        return '9:00';
                        break;
                      case 2:
                        return '12:00';
                        break;
                      case 3:
                        return '15:00';
                        break;
                      case 4:
                        return '18:00';
                        break;
                      default:
                        return "21:00";
                    }
                  },
                  bezierChartScale: BezierChartScale.CUSTOM,
                  xAxisCustomValues: const [0, 1, 2, 3, 4],
                  series: const [
                    BezierLine(
                        label: "心跳",
                        lineColor: Color(0xffeb2f5b),
                        data: const [
                          DataPoint(value: 70, xAxis: 0),
                          DataPoint(value: 90, xAxis: 1),
                          DataPoint(value: 80, xAxis: 2),
                          DataPoint(value: 170, xAxis: 3),
                          DataPoint(value: 85, xAxis: 4),
                        ]),
                  ],
                  config: BezierChartConfig(
                    verticalIndicatorColor: Colors.black26,
                    verticalIndicatorStrokeWidth: 3.0,
                    showVerticalIndicator: true,
                    xAxisTextStyle: TextStyle(
                      color: Color(0xff757575),
                    ),
                    verticalLineFullHeight: true,
                    backgroundColor: Colors.transparent,
                    snap: false,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
