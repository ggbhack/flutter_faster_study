import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';
// import 'package:flutter_faster_study/apps/ui_code_clean/components/build_bottom.dart';

import 'build_progress.dart';
import 'build_water_count.dart';

class BuildWater extends StatelessWidget {
  const BuildWater({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Center(
        child: GlassWidget(
          width: double.infinity,
          height: 450,
          borderRadius: 20,
          color: Color(0xffffffff),
          blur: 5,
          linearGradientBeginOpacity: 0.3,
          linearGradientEndOpacity: 0.2,
          borderGradientBeginOpacity: 0.5,
          borderGradientEndOpacity: 0.02,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset("assets/images/water.png", height: 25),
                            SizedBox(width: 16),
                            Text(
                              "水分",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 16),
                        buildWaterPannel(),
                      ],
                    ),
                    SizedBox(width: 10),
                    BuildProgress(),
                  ],
                ),
                BuildWaterCount(),
              ],
            ),
          ),
        ),
      );

  buildWaterPannel() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "1000ml",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 5),
        Text(
          "每天涉水量",
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 5),
        TextButton.icon(
          onPressed: () {},
          style: TextButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            backgroundColor: Color(0xff43cbcb),
          ),
          icon: Icon(
            Icons.calculate,
            color: Colors.white,
          ),
          label: Text(
            "水分",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }
}
