import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';

class BuildWaterCount extends StatefulWidget {
  const BuildWaterCount({Key key}) : super(key: key);

  @override
  _BuildWaterCountState createState() => _BuildWaterCountState();
}

class _BuildWaterCountState extends State<BuildWaterCount> {
  final datas = [
    true,
    true,
    true,
    true,
    true,
    true,
    false,
    false,
  ];
  @override
  Widget build(BuildContext context) => Container(
        alignment: Alignment.topLeft,
        child: Wrap(
          children:
              List.generate(datas.length, (index) => buildCup(datas[index]))
                  .toList(),
        ),
      );

  Widget buildCup(bool e) => Container(
        margin: EdgeInsets.only(top: 15, right: 15),
        child: GlassWidget(
          width: 60,
          height: 60,
          borderRadius: 10,
          color: Color(0xffffffff),
          blur: 10,
          linearGradientBeginOpacity: 0.7,
          linearGradientEndOpacity: 0.2,
          borderGradientBeginOpacity: 0.5,
          borderGradientEndOpacity: 0.02,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
                child: e
                    ? Image.asset("assets/images/cup.png")
                    : Image.asset("assets/images/cup_out.png")),
          ),
        ),
      );
}
