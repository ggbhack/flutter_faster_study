import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';

class BuildDate extends StatefulWidget {
  const BuildDate({Key key}) : super(key: key);

  @override
  _BuildDateState createState() => _BuildDateState();
}

class _BuildDateState extends State<BuildDate> {
  List<String> dates = ["每天", "每月", "每年"];
  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.only(top: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(
              dates.length,
              (index) => Expanded(
                      child: BuildDateItem(
                    title: dates[index],
                    active: selectedIndex == index,
                    press: () {
                      setState(() {
                        selectedIndex = index;
                      });
                    },
                  ))).toList(),
        ),
      );
}

class BuildDateItem extends StatelessWidget {
  const BuildDateItem({Key key, this.title, this.active, this.press})
      : super(key: key);
  final String title;
  final bool active;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Center(
        child: GlassWidget(
          width: double.infinity,
          height: 50,
          borderRadius: 5,
          color: Color(0xffffffff),
          blur: 10,
          linearGradientBeginOpacity: 0.3,
          linearGradientEndOpacity: 0.2,
          borderGradientBeginOpacity: 0.5,
          borderGradientEndOpacity: 0.02,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              splashColor: Colors.pinkAccent,
              onTap: press,
              child: Center(
                child: Text(
                  title,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: active ? Colors.pinkAccent : Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
