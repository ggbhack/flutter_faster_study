import 'package:flutter/material.dart';
import 'package:glassmorphism/glassmorphism.dart';

class GlassWidget extends StatelessWidget {
  const GlassWidget({
    Key key,
    this.width,
    this.height,
    this.borderRadius,
    this.blur,
    this.linearGradientBeginOpacity,
    this.linearGradientEndOpacity,
    this.borderGradientBeginOpacity,
    this.borderGradientEndOpacity,
    this.child,
    this.color,
  }) : super(key: key);
  final double width;
  final double height;
  final double borderRadius;
  final double blur;
  final double linearGradientBeginOpacity;
  final double linearGradientEndOpacity;
  final double borderGradientBeginOpacity;
  final double borderGradientEndOpacity;
  final Widget child;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GlassmorphicContainer(
        width: width,
        height: height,
        borderRadius: borderRadius,
        blur: blur,
        alignment: Alignment.bottomCenter,
        border: 2,
        linearGradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            color.withOpacity(linearGradientBeginOpacity),
            color.withOpacity(linearGradientEndOpacity),
          ],
          stops: [0.1, 1],
        ),
        borderGradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            color.withOpacity(borderGradientBeginOpacity),
            color.withOpacity(borderGradientEndOpacity),
          ],
        ),
        child: child,
      ),
    );
  }
}
