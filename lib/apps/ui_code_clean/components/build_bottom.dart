/*
 * @Author: your name
 * @Date: 2021-07-27 21:14:05
 * @LastEditTime: 2021-07-28 09:00:12
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\apps\ui_code_clean\components\build_bottom.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/screens/home/home_screen.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/screens/home_01/home_screen.dart';

class BuildBottom extends StatefulWidget {
  const BuildBottom({Key key, this.selected}) : super(key: key);
  final int selected;

  @override
  _BuildBottomState createState() => _BuildBottomState();
}

class _BuildBottomState extends State<BuildBottom> {
  List<IconData> icons = [
    Icons.calendar_today,
    Icons.person_outline_outlined,
    Icons.settings_outlined,
    Icons.file_present
  ];

  @override
  Widget build(BuildContext context) => BottomNavigationBar(
        backgroundColor: Colors.black,
        type: BottomNavigationBarType.shifting,
        selectedItemColor: Color(0xffe82c5c),
        unselectedItemColor: Colors.grey,
        currentIndex: widget.selected,
        onTap: (int value) {
          if (value == 0) {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen(),
              ),
            );
          }
          if (value == 1) {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen1(),
              ),
            );
          }
        },
        items: List.generate(
          icons.length,
          (index) => BottomNavigationBarItem(
              label: "菜单$index",
              icon: Icon(
                icons[index],
                color:
                    index == widget.selected ? Color(0xffe82c5c) : Colors.grey,
              )),
        ).toList(),
      );
}
