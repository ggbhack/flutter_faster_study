/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-30 09:40:19
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-30 11:09:37
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/health_master/model/exercise.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWidget extends StatefulWidget {
  const VideoPlayerWidget(
      {Key key, @required this.exercise, @required this.onInitialized})
      : super(key: key);
  final Exercise exercise;
  final VoidCallback onInitialized;

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  VideoPlayerController controller;

  @override
  void initState() {
    super.initState();
    controller = VideoPlayerController.asset(widget.exercise.videoUrl)
      ..initialize().then((value) {
        controller.setLooping(true);
        controller.play();
        widget.exercise.controller = controller;
        widget.onInitialized();
      });
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
    print("销毁video controller");
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: controller.value.isInitialized
          ? AspectRatio(
              aspectRatio: controller.value.aspectRatio,
              child: VideoPlayer(controller),
            )
          : Center(child: CircularProgressIndicator()),
    );
  }
}
