/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-30 09:53:41
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-30 11:12:59
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/health_master/model/exercise.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';

class VideoControlsWiget extends StatelessWidget {
  const VideoControlsWiget(
      {Key key,
      @required this.exercise,
      @required this.onRewindVideo,
      @required this.onNextVideo,
      @required this.onTogglePlaying})
      : super(key: key);
  final Exercise exercise;
  final VoidCallback onRewindVideo;
  final VoidCallback onNextVideo;
  final ValueChanged<bool> onTogglePlaying;

  @override
  Widget build(BuildContext context) => GlassWidget(
        height: 160,
        borderRadius: 30,
        blur: 3,
        color: Color(0xffffff),
        linearGradientBeginOpacity: 0.3,
        linearGradientEndOpacity: 0.2,
        borderGradientBeginOpacity: 0.5,
        borderGradientEndOpacity: 0.02,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    buildText(
                        title: "坚持住", value: "${exercise.duration.inSeconds}"),
                    SizedBox(height: 10),
                    buildText(title: "重复次数", value: "${exercise.noOfReps}"),
                  ],
                ),
                SizedBox(height: 10),
                buildButtons(context),
              ],
            ),
          ),
        ),
      );

  buildText({@required String title, @required String value}) => Column(
        children: [
          Text(
            title,
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
            ),
          ),
          SizedBox(height: 8),
          Text(value,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
                color: Colors.white,
              )),
        ],
      );

  buildButtons(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            icon: Icon(
              Icons.fast_rewind,
              color: Colors.white,
              size: 32,
            ),
            onPressed: onRewindVideo,
          ),
          buildPlayButton(context),
          IconButton(
            icon: Icon(
              Icons.fast_forward,
              color: Colors.white,
              size: 32,
            ),
            onPressed: onNextVideo,
          ),
        ],
      );

  buildPlayButton(BuildContext context) {
    final isLoading =
        exercise.controller == null || !exercise.controller.value.isInitialized;
    if (isLoading) {
      return Center(child: CircularProgressIndicator());
    } else if (exercise.controller.value.isPlaying) {
      return buildBottom(
        context,
        icon: Icon(Icons.pause, size: 38, color: Colors.white),
        onClicked: () => onTogglePlaying(false),
      );
    } else {
      return buildBottom(
        context,
        icon: Icon(Icons.play_arrow, size: 38, color: Colors.white),
        onClicked: () => onTogglePlaying(true),
      );
    }
  }

  buildBottom(BuildContext context, {Icon icon, void Function() onClicked}) =>
      GestureDetector(
        onTap: onClicked,
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                color: Color(0xffff6369),
                blurRadius: 8,
                offset: Offset(2, 2),
              ),
            ],
          ),
          child: CircleAvatar(
            radius: 24,
            backgroundColor: Color(0xffff6369),
            child: icon,
          ),
        ),
      );
}
