import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';
import 'package:flutter_faster_study/components/custom_showcase_widget.dart';

import 'components/build_lines.dart';
import 'components/exercises_widget.dart';
import 'package:showcaseview/showcaseview.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey _one = GlobalKey();
  GlobalKey _two = GlobalKey();
  GlobalKey _three = GlobalKey();

  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance.addPostFrameCallback(
    //     (_) => ShowCaseWidget.of(context).startShowCase([_one, _two, _three]));
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              "assets/images/bg1.jpg",
              fit: BoxFit.fill,
            ),
            Container(
              margin: EdgeInsets.only(
                top: 12,
                bottom: 12,
              ),
              child: SafeArea(
                child: Center(
                  child: GlassWidget(
                    width: width * 0.94,
                    height: height,
                    borderRadius: 30,
                    blur: 20,
                    color: Color(0xffffff),
                    linearGradientBeginOpacity: 0.3,
                    linearGradientEndOpacity: 0.2,
                    borderGradientBeginOpacity: 0.5,
                    borderGradientEndOpacity: 0.02,
                    child: Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: CustomScrollView(
                        physics: BouncingScrollPhysics(),
                        slivers: [
                          buildAppBar(context),
                          ExercisesWidget(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  buildAppBar(BuildContext context) => SliverAppBar(
        expandedHeight: MediaQuery.of(context).size.height * 0.35,
        flexibleSpace: FlexibleSpaceBar(
          background: BuildLines(),
        ),
        stretch: true,
        title: CustomShowcaseWidget(
          desctiption: '燃脂数据标题',
          globalKey: _one,
          child: Text("燃脂数据"),
        ),
        centerTitle: true,
        pinned: true,
        leading: CustomShowcaseWidget(
          desctiption: '菜单',
          globalKey: _two,
          child: Icon(Icons.menu),
        ),
        actions: [
          CustomShowcaseWidget(
            desctiption: '个人中心',
            globalKey: _three,
            child: Icon(
              Icons.person,
              size: 28,
            ),
          ),
          SizedBox(
            width: 12,
          ),
        ],
      );
}
