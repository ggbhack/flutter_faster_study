import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/health_master/data/exercise_sets.dart';
import 'package:flutter_faster_study/apps/health_master/model/exercise_set.dart';

import 'build_diffIcuty_level.dart';
import 'exercise_set_widget.dart';

class ExercisesWidget extends StatefulWidget {
  const ExercisesWidget({Key key}) : super(key: key);

  @override
  _ExercisesWidgetState createState() => _ExercisesWidgetState();
}

class _ExercisesWidgetState extends State<ExercisesWidget> {
  ExerciseType selectedType = ExerciseType.low;
  @override
  Widget build(BuildContext context) => SliverPadding(
        padding: EdgeInsets.only(bottom: 16, left: 16, right: 16),
        sliver: SliverList(
          delegate: SliverChildListDelegate(
            [
              SizedBox(height: 16),
              Text(
                "运动强度",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
              ),
              SizedBox(height: 8),
              buildDifficutyLevel(),
              buildWorkouts(),
            ],
          ),
        ),
      );

  buildDifficutyLevel() => Row(
        children: ExerciseType.values.map((type) {
          final name = getExerciseName(type);
          final fontWeight =
              selectedType == type ? FontWeight.bold : FontWeight.normal;
          final fontSize = selectedType == type ? 20.0 : 16.0;
          return Expanded(
            child: Center(
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () => setState(() {
                  selectedType = type;
                }),
                child: Text(
                  name,
                  style: TextStyle(
                    fontWeight: fontWeight,
                    fontSize: fontSize,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          );
        }).toList(),
      );

  buildWorkouts() => GestureDetector(
        onHorizontalDragEnd: swiperFunction,
        child: Column(
          children: exerciseSets
              .where((element) => element.exerciseType == selectedType)
              .map(
                (exerciseSet) => Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 10,
                  ),
                  child: ExerciseSetWidget(exerciseSet: exerciseSet),
                ),
              )
              .toList(),
        ),
      );

  void swiperFunction(DragEndDetails details) {
    final selectedIndex = ExerciseType.values.indexOf(selectedType);
    final hasNext = selectedIndex < ExerciseType.values.length - 1;
    final hasPrevious = selectedIndex > 0;
    setState(() {
      if (details.primaryVelocity < 0 && hasNext) {
        final nextType = ExerciseType.values[selectedIndex + 1];
        selectedType = nextType;
      }
      if (details.primaryVelocity > 0 && hasPrevious) {
        final previousType = ExerciseType.values[selectedIndex - 1];
        selectedType = previousType;
      }
    });
  }
}
