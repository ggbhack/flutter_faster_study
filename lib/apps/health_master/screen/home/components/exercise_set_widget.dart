/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-28 22:20:10
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-30 10:39:46
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/health_master/model/exercise_set.dart';
import 'package:flutter_faster_study/apps/health_master/screen/exercise/exercise_screen.dart';
import 'package:flutter_faster_study/apps/ui_code_clean/components/glass_widget.dart';

class ExerciseSetWidget extends StatelessWidget {
  const ExerciseSetWidget({Key key, @required this.exerciseSet})
      : super(key: key);
  final ExerciseSet exerciseSet;

  @override
  Widget build(BuildContext context) => GestureDetector(
        child: GlassWidget(
          width: double.infinity,
          height: 100,
          borderRadius: 20,
          blur: 20,
          color: exerciseSet.color,
          linearGradientBeginOpacity: 0.7,
          linearGradientEndOpacity: 0.4,
          borderGradientBeginOpacity: 0.7,
          borderGradientEndOpacity: 0.4,
          child: InkWell(
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) =>
                    ExerciseScreen(exerciseSet: exerciseSet),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: buildText(),
                  ),
                  Expanded(
                    child: Image.asset(exerciseSet.imageUrl),
                  ),
                ],
              ),
            ),
          ),
        ),
      );

  buildText() {
    final exercises = exerciseSet.exercises;
    final minutes = exerciseSet.totalDuration;
    // final minutes = 5;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          exerciseSet.name,
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        SizedBox(height: 10),
        Text(
          '${exercises.length} 组 $minutes 分钟',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}
