/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-28 22:09:43
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-30 10:39:37
 */
import 'package:flutter/material.dart';

import 'exercise.dart';

enum ExerciseType { low, mid, hard }
String getExerciseName(ExerciseType type) {
  switch (type) {
    case ExerciseType.hard:
      return "剧烈";
      break;
    case ExerciseType.mid:
      return "适中";
      break;
    case ExerciseType.low:
      return "热身";
      break;

    default:
      return "全部";
      break;
  }
}

class ExerciseSet {
  final String name;
  final List<Exercise> exercises;
  final int group;
  final String imageUrl;
  final ExerciseType exerciseType;
  final Color color;
  const ExerciseSet({
    @required this.name,
    @required this.exercises,
    @required this.group,
    @required this.imageUrl,
    @required this.exerciseType,
    @required this.color,
  });

  String get totalDuration {
    final duration = exercises.fold(
        Duration.zero, (previous, element) => previous + element.duration);
    return duration.inMinutes.toString();
  }
}
