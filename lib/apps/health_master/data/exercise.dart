import 'package:flutter_faster_study/apps/health_master/model/exercise.dart';

final exercises1 = [
  Exercise(
    name: "测试视频1",
    duration: Duration(seconds: 30),
    noOfReps: 1,
    videoUrl: "assets/video/1.mp4",
  ),
  Exercise(
    name: "测试视频2",
    duration: Duration(seconds: 30),
    noOfReps: 2,
    videoUrl: "assets/video/2.mp4",
  ),
];

final exercises2 = [
  Exercise(
    name: "测试视频1",
    duration: Duration(seconds: 30),
    noOfReps: 1,
    videoUrl: "assets/video/1.mp4",
  ),
  Exercise(
    name: "测试视频2",
    duration: Duration(seconds: 30),
    noOfReps: 2,
    videoUrl: "assets/video/2.mp4",
  ),
  Exercise(
    name: "测试视频3",
    duration: Duration(seconds: 30),
    noOfReps: 3,
    videoUrl: "assets/video/3.mp4",
  ),
];

final exercises3 = [
  Exercise(
    name: "测试视频1",
    duration: Duration(seconds: 30),
    noOfReps: 1,
    videoUrl: "assets/video/1.mp4",
  ),
  Exercise(
    name: "测试视频2",
    duration: Duration(seconds: 30),
    noOfReps: 2,
    videoUrl: "assets/video/2.mp4",
  ),
  Exercise(
    name: "测试视频3",
    duration: Duration(seconds: 30),
    noOfReps: 3,
    videoUrl: "assets/video/3.mp4",
  ),
  Exercise(
    name: "测试视频4",
    duration: Duration(seconds: 30),
    noOfReps: 4,
    videoUrl: "assets/video/4.mp4",
  ),
];

final exercises4 = [
  Exercise(
    name: "测试视频1",
    duration: Duration(seconds: 30),
    noOfReps: 1,
    videoUrl: "assets/video/1.mp4",
  ),
  Exercise(
    name: "测试视频2",
    duration: Duration(seconds: 30),
    noOfReps: 2,
    videoUrl: "assets/video/2.mp4",
  ),
  Exercise(
    name: "测试视频3",
    duration: Duration(seconds: 30),
    noOfReps: 3,
    videoUrl: "assets/video/3.mp4",
  ),
  Exercise(
    name: "测试视频4",
    duration: Duration(seconds: 30),
    noOfReps: 4,
    videoUrl: "assets/video/4.mp4",
  ),
  Exercise(
    name: "测试视频5",
    duration: Duration(seconds: 30),
    noOfReps: 5,
    videoUrl: "assets/video/5.mp4",
  ),
];
final exercises5 = [
  Exercise(
    name: "测试视频1",
    duration: Duration(seconds: 30),
    noOfReps: 1,
    videoUrl: "assets/video/1.mp4",
  ),
  Exercise(
    name: "测试视频2",
    duration: Duration(seconds: 30),
    noOfReps: 2,
    videoUrl: "assets/video/2.mp4",
  ),
  Exercise(
    name: "测试视频3",
    duration: Duration(seconds: 30),
    noOfReps: 3,
    videoUrl: "assets/video/3.mp4",
  ),
  Exercise(
    name: "测试视频4",
    duration: Duration(seconds: 30),
    noOfReps: 4,
    videoUrl: "assets/video/4.mp4",
  ),
  Exercise(
    name: "测试视频5",
    duration: Duration(seconds: 30),
    noOfReps: 5,
    videoUrl: "assets/video/5.mp4",
  ),
  Exercise(
    name: "测试视频6",
    duration: Duration(seconds: 30),
    noOfReps: 6,
    videoUrl: "assets/video/6.mp4",
  ),
];
