import 'package:flutter/material.dart';
import 'package:fijkplayer/fijkplayer.dart';

class RtspVideo extends StatefulWidget {
  const RtspVideo({Key key, this.url}) : super(key: key);
  final String url;

  @override
  _RtspVideoState createState() => _RtspVideoState();
}

class _RtspVideoState extends State<RtspVideo> {
  final FijkPlayer player = FijkPlayer();

  @override
  void initState() {
    super.initState();
    setState(() {
      player.setDataSource(widget.url, autoPlay: true);
    });
  }

  @override
  void dispose() {
    super.dispose();
    player.release();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          FijkView(player: player, width: double.infinity, height: 300),
          TextButton(
            child: Text("播放"),
            onPressed: () {
              player.start();
            },
          ),
          TextButton(
            child: Text("暂停"),
            onPressed: () {
              setState(() {
                player.pause();
              });
            },
          ),
          TextButton(
            child: Text("清除"),
            onPressed: () {
              setState(() {
                player.stop();
              });
            },
          ),
        ],
      ),
    );
  }
}
