/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-02 16:50:47
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-02 18:57:51
 */
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_faster_study/apps/video_test/components/video.dart';
import 'package:flutter_faster_study/apps/video_test/utils/text_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController controller;
  bool isChange = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    final text = TextPreferences.getText();
    setState(() {
      controller = TextEditingController(text: text);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("视频rtsp测试"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              isChange ? Container() : RtspVideo(url: controller.text),
              SizedBox(height: 30),
              TextFormField(
                maxLines: 5,
                controller: controller,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "请输入播放地址",
                ),
                onChanged: (value) {
                  if (value != null) {
                    //打印内容
                    TextPreferences.setText(value);
                  }
                },
              ),
              TextButton(
                child: Text("粘贴地址"),
                onPressed: () async {
                  var clipboardData =
                      await Clipboard.getData(Clipboard.kTextPlain); //获取粘贴板中的文本
                  print(clipboardData.text);
                  if (clipboardData != null) {
                    //打印内容
                    setState(() {
                      isChange = true;
                    });
                    Future.delayed(Duration(milliseconds: 2000), () {
                      print("切换视频");
                      setState(() {
                        controller =
                            TextEditingController(text: clipboardData.text);
                        isChange = false;
                      });
                    });
                    TextPreferences.setText(clipboardData.text);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
