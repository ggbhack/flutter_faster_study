import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'data.dart';
import 'page_indicator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'onBoading',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  PageController _controller;
  AnimationController animationController;
  Animation<double> _scaleAnimation;
  int currentPage = 0;
  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      duration: Duration(milliseconds: 300),
      vsync: this,
    );
    _controller = new PageController(initialPage: currentPage);
    _scaleAnimation = Tween(begin: 0.6, end: 1.0).animate(animationController);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [Color(0xFF485563), Color(0xff29323c)],
            tileMode: TileMode.clamp,
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0.0, 1.0]),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          fit: StackFit.expand,
          children: [
            PageView.builder(
              physics: BouncingScrollPhysics(),
              controller: _controller,
              itemCount: pageList.length,
              onPageChanged: (index) {
                setState(() {
                  currentPage = index;
                  isLastPage =
                      currentPage == pageList.length - 1 ? true : false;
                  currentPage == pageList.length - 1
                      ? animationController.forward()
                      : animationController.reverse();
                });
              },
              itemBuilder: (context, index) {
                return AnimatedBuilder(
                  animation: _controller,
                  builder: (context, child) {
                    PageModel page = pageList[index];
                    var delta;
                    double y = 1.0;
                    if (_controller.position.haveDimensions) {
                      delta = _controller.page - index;
                      y = 1 - delta.abs().clamp(0.0, 1.0);
                    }
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(page.imageUrl),
                        Container(
                          margin: EdgeInsets.only(left: 12),
                          height: 100.0,
                          child: Stack(
                            children: [
                              Opacity(
                                opacity: 0.10,
                                child: GradientText(
                                  page.title,
                                  gradient: LinearGradient(
                                    colors: page.titleGradient,
                                  ),
                                  style: TextStyle(
                                      letterSpacing: 1.0,
                                      fontFamily: "Black",
                                      fontWeight: FontWeight.w500,
                                      fontSize: 90.0),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 30.0, left: 22.0),
                                child: GradientText(
                                  page.title,
                                  gradient: LinearGradient(
                                    colors: page.titleGradient,
                                  ),
                                  style: TextStyle(
                                      letterSpacing: 1.0,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "Black",
                                      fontSize: 70.0),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 34.0, top: 12.0),
                          child: Transform(
                            transform:
                                Matrix4.translationValues(0, 50.0 * (1 - y), 0),
                            child: Text(
                              page.body,
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Color(0xff9b9b9b),
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                );
              },
            ),
            Positioned(
              left: 30.0,
              bottom: 55.0,
              child: Container(
                width: 160,
                child: PageIndicator(
                    currentIndex: currentPage, pageCount: pageList.length),
              ),
            ),
            Positioned(
              right: 30.0,
              bottom: 30.0,
              child: ScaleTransition(
                scale: _scaleAnimation,
                child: isLastPage
                    ? FloatingActionButton(
                        backgroundColor: Colors.white,
                        child: Icon(
                          Icons.arrow_forward,
                          color: Colors.black,
                        ),
                        onPressed: () {},
                      )
                    : Container(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
