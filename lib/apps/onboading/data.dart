import 'package:flutter/material.dart';

List pageList = [
  PageModel(
    imageUrl: 'assets/images/icon.png',
    title: "Por 959",
    body: "集合众多科技与一身百公里时速3.7秒",
    titleGradient: gradient[0],
  ),
  PageModel(
    imageUrl: 'assets/images/icon.png',
    title: "LANCIA",
    body: "蓝旗亚是菲利特集团旗下的品牌之一",
    titleGradient: gradient[1],
  ),
  PageModel(
    imageUrl: 'assets/images/icon.png',
    title: "Por 977",
    body: "那美丽的外形让人很容易对她一见钟情",
    titleGradient: gradient[2],
  ),
];

List<List<Color>> gradient = [
  [Color(0xff9708cc), Color(0xff43cbff)],
  [Color(0xffe2859f), Color(0xfffccf31)],
  [Color(0xff5efce8), Color(0xff736efe)],
];

class PageModel {
  String imageUrl;
  String title;
  String body;
  List<Color> titleGradient = [];
  PageModel({this.imageUrl, this.title, this.body, this.titleGradient});
}
