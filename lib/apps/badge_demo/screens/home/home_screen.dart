import 'package:badges/badges.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int index = 0;
  int countFavourites = 98;
  int countMessages = 9;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("徽章"),
      ),
      bottomNavigationBar: buildBottomBar(),
      body: Container(
        padding: EdgeInsets.all(32),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Badge(
                toAnimate: true,
                badgeColor: Colors.teal,
                padding: EdgeInsets.all(8),
                badgeContent: Text(
                  '$countFavourites',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                child: buildButton(
                  text: "添加收藏",
                  onClicked: () => setState(() => countMessages += 1),
                ),
              ),
              const SizedBox(height: 32),
              Badge(
                position: BadgePosition.topStart(),
                toAnimate: true,
                badgeColor: Colors.teal,
                padding: EdgeInsets.all(8),
                badgeContent: Text(
                  '$countFavourites',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                child: buildButton(
                  text: "添加消息",
                  onClicked: () => setState(() => countFavourites += 1),
                ),
              ),
              const SizedBox(height: 32),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildBottomBar() {
    final style = TextStyle(color: Colors.black);
    return BottomNavigationBar(
      backgroundColor: Theme.of(context).primaryColor,
      selectedItemColor: Colors.black,
      unselectedItemColor: Colors.black54,
      currentIndex: index,
      items: [
        BottomNavigationBarItem(
            icon: buildCustomBadge(
              counter: countFavourites,
              child: Icon(Icons.favorite_border),
            ),
            label: '收藏'),
        BottomNavigationBarItem(icon: Icon(Icons.message), label: '消息'),
      ],
      onTap: (int index) => setState(() {
        this.index = index;
      }),
    );
  }

  buildButton({String text, VoidCallback onClicked}) => ElevatedButton(
        child: Text(
          text,
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        onPressed: onClicked,
        style: ElevatedButton.styleFrom(
          minimumSize: Size.fromHeight(50),
          primary: Colors.pink,
        ),
      );

  Widget buildCustomBadge({@required int counter, @required Widget child}) {
    final text = counter.toString();
    final deltaFontSize = (text.length - 1) * 3.0;
    return Stack(
      clipBehavior: Clip.hardEdge,
      children: [
        child,
        Positioned(
          top: -6,
          right: -20,
          child: CircleAvatar(
            backgroundColor: Colors.white,
            child: Text(
              text,
              style: TextStyle(
                fontSize: 16 - deltaFontSize,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        )
      ],
    );
  }
}
