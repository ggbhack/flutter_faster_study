/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-29 16:58:39
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-29 18:05:31
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/auth_face_finger/api/local_auth_api.dart';
import 'package:line_icons/line_icons.dart';
import 'package:local_auth/local_auth.dart';

import 'home_screen.dart';

class FingerprintScreen extends StatelessWidget {
  const FingerprintScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Icon(LineIcons.medal, color: Colors.black),
        title: Text("健康管家", style: TextStyle(color: Colors.black)),
        centerTitle: true,
        actions: [
          Icon(LineIcons.calculator, color: Colors.black),
          SizedBox(width: 16),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(32),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox.fromSize(
                size: Size.fromHeight(100),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Spacer(),
                    Icon(LineIcons.fingerprint),
                    Text("or"),
                    Icon(LineIcons.qrcode),
                    Spacer(),
                  ],
                ),
              ),
              SizedBox(height: 50),
              buildAvailability(context),
              SizedBox(height: 24),
              buildAuthenticate(context),
            ],
          ),
        ),
      ),
    );
  }

  buildAvailability(BuildContext context) => buildButton(
        text: '检测生物识别',
        icon: Icons.event_available,
        onClicked: () async {
          // 获取设备是否支持生物识别功能
          final isAvailable = await LocalAuthApi.hasBiometrics();
          final biometrics = await LocalAuthApi.getBiometrics();
          final hasFingerprint = biometrics.contains(BiometricType.fingerprint);
          final hasFace = biometrics.contains(BiometricType.face);
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text("是否支持生物识别"),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  buildText('生物识别', isAvailable),
                  buildText('指纹识别', hasFingerprint),
                  buildText('脸部识别', hasFace),
                ],
              ),
            ),
          );
        },
      );

  buildAuthenticate(BuildContext context) => buildButton(
        text: '权限验证',
        icon: Icons.lock_open,
        onClicked: () async {
          final isAuthenticated = await LocalAuthApi.authenticate();
          if (isAuthenticated) {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen(),
              ),
            );
          }
        },
      );

  buildButton({String text, IconData icon, Future<Null> Function() onClicked}) {
    return TextButton(
      onPressed: onClicked,
      child: Row(
        children: [Icon(icon), Text(text)],
      ),
    );
  }

  buildText(String text, bool checked) => Container(
        margin: EdgeInsets.symmetric(
          vertical: 8,
        ),
        child: Row(
          children: [
            checked
                ? Icon(Icons.check, color: Colors.green, size: 24)
                : Icon(Icons.close, color: Colors.red, size: 24),
            const SizedBox(width: 12),
            Text(
              text,
              style: TextStyle(fontSize: 24),
            ),
          ],
        ),
      );
}
