/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-29 16:00:02
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-29 17:37:17
 */
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'home/fingerprint_screen.dart';

// import 'screens/home_01/home_screen.dart'; //第二个玻璃屏效果

// void main() {
//   runApp(MyApp());
// }

Future main() async {
  // 初始化
  WidgetsFlutterBinding.ensureInitialized();
  // 设置横竖屏 锁定方向
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '获取widget的大小和位置',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xffe82c5c),
      ),
      home: FingerprintScreen(),
    );
  }
}
