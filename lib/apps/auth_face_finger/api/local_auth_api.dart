/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-29 16:32:20
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-29 17:52:56
 */
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';

class LocalAuthApi {
  static final _auth = LocalAuthentication();
  // 获取生物识别功能
  static Future<bool> authenticate() async {
    // 判断设备是否支持生物识别功能
    final isAvailable = await hasBiometrics();
    if (!isAvailable) return false;
    try {
      return await _auth.authenticateWithBiometrics(
        localizedReason: "请进行指纹识别",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      return false;
    }
  }

  static Future<bool> hasBiometrics() async {
    // 调用这个方法  判断设备是否支持生物识别功能
    try {
      return await _auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      return false;
    }
  }

  // 获取设备支持生物识别列表
  static Future<List<BiometricType>> getBiometrics() async {
    try {
      return await _auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      return <BiometricType>[];
    }
  }
}
