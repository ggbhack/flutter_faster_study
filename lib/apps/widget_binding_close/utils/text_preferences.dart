/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-02 17:09:53
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-02 17:12:37
 */
/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-30 15:35:22
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-02 17:07:32
 */
import 'package:shared_preferences/shared_preferences.dart';

class TextPreferences {
  static const _keyText = "text";

  // 初始化
  static SharedPreferences _preferences;
  static Future init() async =>
      _preferences = await SharedPreferences.getInstance();
// 获取输入框的信息
  static Future setText(String text) async =>
      await _preferences.setString(_keyText, text);
  static String getText() => _preferences.getString(_keyText) ?? '';
}
