import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text("flutter方向控制"),
          actions: [
            IconButton(
              icon: Icon(Icons.clear),
              onPressed: setPortraintAndLandscape,
            ),
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(32),
          // orientation 包含两个方向一个水平，一个垂直
          child: OrientationBuilder(
            builder: (context, orientation) =>
                orientation == Orientation.portrait
                    ? buildPortrait()
                    : buildLandscape(),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.rotate_left),
          onPressed: () {
            final isPortrait =
                MediaQuery.of(context).orientation == Orientation.portrait;
            isPortrait ? setLandscape() : setPortrait();
          },
        ),
      );
// 设置竖屏
  Future setPortrait() async => await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  // 设置横屏
  Future setLandscape() async => await SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);

  Future setPortraintAndLandscape() =>
      SystemChrome.setPreferredOrientations(DeviceOrientation.values);

  buildPortrait() => ListView(
        children: [
          buildImage(),
          const SizedBox(height: 16),
          buildText(),
        ],
      );

  buildImage() => Image.network(
      "https://cdn.pixabay.com/photo/2021/07/26/14/30/woman-6494461_960_720.jpg");

  buildText() => Column(
        children: [
          Text(
            "随意的人生",
            style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 20),
          Text(
            "　生活本不苦，苦的是欲望过多，身心本无累，累的是背负太多，再苦，都要用今天的微笑，把它吟咏成一段从容的记忆，再累，都要用当下的遗忘穿越万道红尘，让心波澜不惊，认识一个人靠缘分，了解一个人靠耐心，征服一个人靠智慧，处好一个人靠包容。生活本来就不完美，用一颗平淡的心看待周围的一切，人生，活得就是一种心情，惟愿天天开心，谁都不是完美的人，都渴望完美，寂寞的时候，鼓励自己，努力才会有所成就，孤独的时刻，时时告诫自己，追求才能有所收获，失意时，不会放弃，得意时，不会骄横，善待生活，守望未来，给自己一个安慰，心安，静好。人的一生不长，简单生活不难，有些事，别问别人为什么，多问自己凭什么，凡事都从自己找原因，不为失败怪别人，别做太多自己不想做的事，别只顾及别人的感受，却委屈了自己，抓住属于自己的，从现在开始，不在为任何人活着，只为自己人生理想而做，人不需要多少辉煌，不屈求什么，自己开心，过的随意，一个好的心态，做人拿得起，做事放得下，与人处事合得来，人生在世，有得就有失，有付出就有回报，鱼和熊掌不能兼得，有时你的付出不一定能得到回报，但自己要想明白一些，不要太苛求自己，生命总有它的轮回，上帝是公平的，它对每个人都是一样的垂青，人生苦短，好好的潇洒做一回自己，追求自己的路，过好自己生活。让自己的心灵有一份纯净的湖泊，累了在上面泛泛舟，洗涤风尘创伤，有个朋友，财富不是一个人一生的朋友，而朋友有时则是你一生的财富，人人都希望有朋友，没有朋友的人是可悲又可怜的，但要想有一个真心的朋友又是很难的，朋友不在多而在精，所谓人生得一知己足以，君子之交淡如水，小人之交酒肉亲，就是这个道理。人生活的就是一种心情，穷富也好，得失也好，成败也好，都如云烟，风吹即散，人生，是不断收获与放弃的过程，要得之坦然，失之淡然，成不骄，败不馁，不仰望别人的辉煌，不羡慕别人光芒，把握真实的自己，活出自己的本色，听自己内心的感受，只要心是踏实的，日子就是快乐的，生活就是真实，人生就要随意。人生简单，善待别人，理解自己，不要玩心计，不要算计人，其实世界并不复杂，复杂的是人心，是复杂的人心让这个世界变得复杂了，人生没必要自寻烦恼，真诚地对待身边的每一个人，微笑着面对每天的生活，做好自己该做的，欲望少一点，自由多一些，过自己的生活，走自己的路，做简简单单的自己，又何愁不会快乐呢。对生活要热忱，对人生要有所追求，得意看淡，失意看开，生命的旅途，有坎坷就会有历练，有浮沉就会有懂得，有阳光就会有希望，有雨落就会有诗意，人间三千事，淡然一笑间，面对人生种种境遇，一笑而过，是一种人生的优雅，活着，说简单其实很简单，笑看得失才会海阔天空，心有透明才会春暖花开。生活要珍惜，人生多努力，人生该干的要干，该退的要退，是一种睿智，人生该显的要显，该藏的要藏，是一种境界，可以相信别人，但不可以指望别人，不要拒绝善意，无功不受大禄，无助不受大礼，无胆不挣大钱，常与高人交往，闲与雅人相会，乐与亲人分享，人生务须多富有，开心温饱人自由，不想去赚多大钱，做人还是多善良。现实人生，感受生活，时光转换，体会到缘分善变，平淡无语，感受了人情冷暖，有心的人，不管你在与不在，都会惦念，无心的情，无论你好与不好，只是漠然，无论繁华与平淡，都是属于自己的风景，要学会面对，人生，何必负赘太多，想开，看开，放开，如此而已，平淡最美，清欢最真，善待生活多努力，珍惜人生要随意。",
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),
          ),
        ],
      );

  buildLandscape() => Row(
        children: [
          buildImage(),
          const SizedBox(width: 16),
          Expanded(
            child: SingleChildScrollView(
              child: buildText(),
            ),
          ),
        ],
      );
}
