import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'screen/home_screen.dart';

// void main() {
//   runApp(MyApp());
// }

Future main() async {
  // 初始化
  WidgetsFlutterBinding.ensureInitialized();
  // 设置横竖屏 锁定方向
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '体能管家',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xffe82c5c),
      ),
      home: HomeScreen(),
    );
  }
}
