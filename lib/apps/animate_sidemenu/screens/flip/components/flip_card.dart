/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-02 15:53:33
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-02 16:23:26
 */
import 'dart:math';

import 'package:flutter/material.dart';

class FlipCard extends StatefulWidget {
  const FlipCard({Key key}) : super(key: key);

  @override
  _FlipCardState createState() => _FlipCardState();
}

class _FlipCardState extends State<FlipCard>
    with SingleTickerProviderStateMixin {
  bool _isFirst = true;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _isFirst = !_isFirst;
        });
      },
      child: AnimatedSwitcher(
        child: _isFirst ? _frontCard : _rearCard,
        duration: Duration(seconds: 1),
        transitionBuilder: (Widget child, Animation<double> animation) {
          final rotate = Tween(begin: pi, end: 0.0).animate(animation);
          return AnimatedBuilder(
              animation: rotate,
              child: child,
              builder: (context, child) {
                final angle = (ValueKey(_isFirst) != widget.key)
                    ? min(rotate.value, pi / 2)
                    : rotate.value;
                return Transform(
                  transform: Matrix4.rotationY(angle),
                  child: child,
                  alignment: Alignment.center,
                );
              });
        },
        switchInCurve: Curves.easeInCubic,
        switchOutCurve: Curves.easeInOutCubic,
      ),
    );
  }
  // @override
  // Widget build(BuildContext context) {
  //   return Column(
  //     children: [
  //       RaisedButton(
  //         onPressed: () {
  //           setState(() {
  //             _isFirst = !_isFirst;
  //           });
  //         },
  //       ),
  //       AnimatedSwitcher(
  //         duration: Duration(seconds: 1),
  //         child: _isFirst ? _frontCard : _rearCard,
  //         transitionBuilder: (Widget child, Animation<double> value) {
  //           return ScaleTransition(
  //             child: child,
  //             scale: value,
  //           );
  //         },
  //       )
  //     ],
  //   );
  // }

  Widget _frontCard = Container(
    key: ValueKey(true),
    color: Colors.orangeAccent,
    width: 200,
    height: 200,
  );

  Widget _rearCard = Container(
    key: ValueKey(false),
    color: Colors.blue,
    width: 200,
    height: 200,
  );

  Widget _currChild = Container(
    key: ValueKey("1"),
    height: 300,
    width: 300,
    color: Colors.red,
  );
}
