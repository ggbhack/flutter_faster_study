/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-02 15:46:36
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-02 16:04:18
 */
import 'package:flutter/material.dart';

import 'components/flip_card.dart';

class FlipScreen extends StatelessWidget {
  const FlipScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("翻转动画")),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FlipCard(),
          ],
        ),
      ),
    );
  }
}
