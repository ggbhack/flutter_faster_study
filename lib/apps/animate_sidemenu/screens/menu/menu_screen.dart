/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-02 07:08:18
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-02 15:33:53
 */
import 'package:flutter/material.dart';

import 'components/side_menu.dart';

class MenuScreen extends StatefulWidget {
  const MenuScreen({Key key}) : super(key: key);

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  final _data = List<String>.generate(8, (index) {
    if (index == 0) {
      return 'assets/images/close.png';
    }
    return 'assets/images/icon_$index.png';
  });
  int _selectedIndex = 1;
  @override
  Widget build(BuildContext context) {
    return SideMenu(
      onItemSelected: (index) {
        if (index != 0) {
          setState(() {
            _selectedIndex = index;
          });
        }
      },
      builder: (VoidCallback showMenu) {
        return Scaffold(
          body: _getWidget(showMenu),
        );
      },
      items: _data.map((e) {
        return Padding(
          padding: const EdgeInsets.all(10),
          child: Image.asset(e),
        );
      }).toList(),
      selectedColor: Color(0xff365642).withOpacity(0.9),
      unselectedColor: Colors.white.withOpacity(0.9),
    );
  }

  Widget _getWidget(VoidCallback showMenu) {
    String value;
    if (_selectedIndex.isOdd) {
      value = 'assets/images/bg1.jpg';
    } else {
      value = 'assets/images/bg.jpg';
    }
    return Container(
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.topLeft,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
              icon: Icon(
                // CommunityMaterialIcons.text,
                Icons.menu_open,
                color: Colors.blue,
                size: 32,
              ),
              onPressed: showMenu,
            ),
            IconButton(
              icon: Icon(
                // CommunityMaterialIcons.yoga,
                Icons.pedal_bike,
                color: Colors.blue,
                size: 32,
              ),
              onPressed: () {},
            ),
          ],
        ),
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
        fit: BoxFit.cover,
        image: AssetImage(value),
      )),
    );
  }
}
