import 'package:flutter/material.dart';

// import 'package:community_material_icon/community_material_icon.dart';
typedef SideMenuBuilder = Widget Function(VoidCallback);

class SideMenu extends StatefulWidget {
  final SideMenuBuilder builder;
  final List<Widget> items;
  final ValueChanged<int> onItemSelected;
  final Color selectedColor;
  final Color unselectedColor;
  const SideMenu(
      {Key key,
      this.builder,
      this.items,
      this.onItemSelected,
      this.selectedColor,
      this.unselectedColor})
      : super(key: key);

  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  List<Animation<double>> _animation;
  int _selectedIndex = 1;

  @override
  void initState() {
    super.initState();
    _animationController = new AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 700),
    );
    final _intervalGap = 1 / widget.items.length;
    _animation = List.generate(
      widget.items.length,
      (index) => Tween(
        begin: 0.0,
        end: 1.6,
      ).animate(
        // 交织动画
        CurvedAnimation(
          parent: _animationController,
          curve: Interval(
            index * _intervalGap,
            index * _intervalGap + _intervalGap,
          ),
        ),
      ),
    );
    // 一开始 隐藏菜单栏
    _animationController.forward(from: 1.0);
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: LayoutBuilder(
        builder: (context, constraints) {
          final itemSize = constraints.maxHeight / widget.items.length;
          final width = 120.0;
          return SafeArea(
            child: AnimatedBuilder(
              animation: _animationController,
              builder: (context, child) => Stack(
                children: [
                  widget.builder(() {
                    _animationController.reverse();
                  }),
                  for (int i = 0; i < widget.items.length; i++)
                    Positioned(
                      left: 0,
                      top: itemSize * i,
                      width: width,
                      height: itemSize,
                      child: Transform(
                        transform: Matrix4.identity()
                          ..setEntry(3, 2, 0.001)
                          ..rotateY(_animationController.status ==
                                  AnimationStatus.reverse
                              ? -_animation[widget.items.length - 1 - i].value
                              : -_animation[i].value),
                        alignment: Alignment.bottomLeft,
                        child: Material(
                          color: i == _selectedIndex
                              ? widget.selectedColor
                              : widget.unselectedColor,
                          child: InkWell(
                            onTap: () {
                              widget.onItemSelected(i);
                              if (i != 0) {
                                setState(() {
                                  _selectedIndex = i;
                                });
                              }
                              _animationController.forward(from: 0.0);
                            },
                            child: widget.items[i],
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
          );
          ;
        },
      ),
    );
  }
}
