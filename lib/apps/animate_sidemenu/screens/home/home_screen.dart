/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-02 07:00:00
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-02 15:47:24
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/animate_sidemenu/screens/flip/flip_screen.dart';
import 'package:flutter_faster_study/apps/animate_sidemenu/screens/menu/menu_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("边框导航栏"),
      ),
      body: ListView(
        padding: EdgeInsets.all(10),
        children: [
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => FlipScreen(),
                ),
              );
            },
            child: Text("翻转动画"),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => MenuScreen(),
                ),
              );
            },
            child: Text("自定义边栏导航"),
          ),
        ],
      ),
    );
  }
}
