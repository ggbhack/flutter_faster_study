/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-30 11:12:22
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-30 11:30:44
 */
import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';

import 'package:get/get.dart';

import 'controller/controller.dart';
import 'screens/home/home_screen.dart';

void main() {
  runApp(MyApp());
}

// Future main() async {
//   // 初始化
//   WidgetsFlutterBinding.ensureInitialized();
//   // 设置横竖屏 锁定方向
//   await SystemChrome.setPreferredOrientations([
//     DeviceOrientation.portraitUp,
//     DeviceOrientation.portraitDown,
//   ]);

//   runApp(MyApp());
// }

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final ThemeController mode = Get.put(ThemeController());
    return GetMaterialApp(
      title: '主题切换',
      debugShowCheckedModeBanner: false,
      theme: mode.defaultTheme,
      home: HomeScreen(),
    );
  }
}
