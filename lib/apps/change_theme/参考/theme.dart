/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-30 11:34:45
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-30 11:34:53
 */
// import 'package:chat_message_app/constants.dart';
// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';

// ThemeData lightThemeData(BuildContext context) {
//   return ThemeData.light().copyWith(
//     primaryColor: kPrimaryColor, //主题色
//     scaffoldBackgroundColor: Colors.white, // scaffold的背景颜色
//     appBarTheme: appBarTheme,
//     iconTheme: IconThemeData(color: kContentColorLightTheme),
//     textTheme: GoogleFonts.interTextTheme(Theme.of(context).textTheme)
//         .apply(bodyColor: kContentColorLightTheme),
//     colorScheme: ColorScheme.light(
//       primary: kPrimaryColor,
//       secondary: kSecondaryColor,
//       error: kErrorColor,
//     ),
//     bottomNavigationBarTheme: BottomNavigationBarThemeData(
//       backgroundColor: Colors.white,
//       selectedItemColor: kContentColorLightTheme.withOpacity(0.7),
//       unselectedItemColor: kContentColorLightTheme.withOpacity(0.32),
//       selectedIconTheme: IconThemeData(color: kPrimaryColor),
//       showUnselectedLabels: true,
//     ),
//   );
// }

// ThemeData darkThemeData(BuildContext context) {
//   return ThemeData.dark().copyWith(
//     primaryColor: kPrimaryColor, //主题色
//     scaffoldBackgroundColor: kContentColorLightTheme, // scaffold的背景颜色
//     appBarTheme: appBarTheme,
//     iconTheme: IconThemeData(color: kContentColorDarkTheme),
//     textTheme: GoogleFonts.interTextTheme(Theme.of(context).textTheme)
//         .apply(bodyColor: kContentColorDarkTheme),
//     colorScheme: ColorScheme.dark(
//       primary: kPrimaryColor,
//       secondary: kSecondaryColor,
//       error: kErrorColor,
//     ),
//     bottomNavigationBarTheme: BottomNavigationBarThemeData(
//       backgroundColor: kContentColorLightTheme,
//       selectedItemColor: kContentColorDarkTheme.withOpacity(0.7),
//       unselectedItemColor: kContentColorDarkTheme.withOpacity(0.32),
//       selectedIconTheme: IconThemeData(color: kPrimaryColor),
//       showUnselectedLabels: true,
//     ),
//   );
// }

// final appBarTheme = AppBarTheme(centerTitle: false, elevation: 0);
