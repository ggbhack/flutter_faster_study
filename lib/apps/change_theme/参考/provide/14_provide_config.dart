// import 'package:api/pages/apps/changeThemeDemo/14_theme_config.dart';
// import 'package:provide/provide.dart';

// class ProvideConfig {
//   // 初始化provide 以及themeconfig主题
//   static init({child, dispose = false}) {
//     final providers = Providers()..provide(Provider.value(ThemeConfig()));
//     // 【child】需要进行状态管理的子wigdet
//     // 【providers】状态管理器
//     return ProviderNode(child: child, providers: providers, dispose: dispose);
//   }

//   // 通过provide小部件获取状态封装
//   static connect<T>({builder, child, scope}) {
//     return Provide<T>(builder: builder, child: child, scope: scope);
//   }

//   // 通过Provide.value<T>(context) 获取封装
//   static T value<T>(context, {scope}) {
//     return Provide.value<T>(context, scope: scope);
//   }
// }
