// import 'package:api/pages/apps/changeThemeDemo/14_provide_config.dart';
// import 'package:api/pages/apps/changeThemeDemo/14_theme_config.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';

// void main() => runApp(ProvideConfig.init(child: MyThemeApp()));

// class MyThemeApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return ProvideConfig.connect<ThemeConfig>(
//         builder: (content, child, ThemeConfig model) {
//       // 每次通知数据刷新时，builder都会刷新该小部件
//       return MaterialApp(theme: model.defaultTheme, home: MyHomePage());
//     });
//   }
// }

// class MyHomePage extends StatefulWidget {
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('应用主题切换'),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             FlatButton(
//                 onPressed: () {
//                   ProvideConfig.value<ThemeConfig>(context).setTheme(0);
//                   print("点击了亮色");
//                 },
//                 child: Text('亮色')),
//             FlatButton(
//                 onPressed: () {
//                   ProvideConfig.value<ThemeConfig>(context).setTheme(1);
//                   print("点击了暗色");
//                 },
//                 child: Text('暗色')),
//             FlatButton(
//                 onPressed: () {
//                   ProvideConfig.value<ThemeConfig>(context).setTheme(2);
//                   print("点击了紫色");
//                 },
//                 child: Text('紫色')),
//           ],
//         ),
//       ),
//     );
//   }
// }
