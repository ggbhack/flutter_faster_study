/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-30 10:52:39
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-30 10:53:21
 */
import 'package:flutter/material.dart';

class ThemeConfig with ChangeNotifier {
  // 亮色主题，应用程序默认主题
  final ThemeData lightTheme = ThemeData(
      // 亮色
      brightness: Brightness.light,
      // 主背景色
      primaryColor: Colors.blue);

  // 暗色主题，应用程序默认主题
  final ThemeData darkTheme = ThemeData(
      // 暗色
      brightness: Brightness.dark,
      // 主背景色
      primaryColor: Colors.grey);
  // 紫色主题
  final ThemeData violetTheme = ThemeData(
      // 亮色
      brightness: Brightness.light,
      // 主背景色
      primaryColor: Colors.deepPurple);

  // 程序已进入的默认主题，应用程序默认主题
  ThemeData defaultTheme = ThemeData(
      // 亮色
      brightness: Brightness.light,
      // 主背景色
      primaryColor: Colors.blue);

  void setTheme(num index) {
    switch (index) {
      case 0:
        defaultTheme = lightTheme;
        break;
      case 1:
        defaultTheme = darkTheme;
        break;
      case 2:
        defaultTheme = violetTheme;
        break;
      default:
    }
    notifyListeners();
  }
}
