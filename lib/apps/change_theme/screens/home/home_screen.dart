/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-30 11:13:59
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-30 11:32:39
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_faster_study/apps/change_theme/controller/controller.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('应用主题切换'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
                onPressed: () {
                  Get.find<ThemeController>().setTheme(0);
                  // ProvideConfig.value<ThemeConfig>(context).setTheme(0);
                  print("点击了亮色");
                },
                child: Text('亮色')),
            TextButton(
                onPressed: () {
                  Get.find<ThemeController>().setTheme(1);
                  // ProvideConfig.value<ThemeConfig>(context).setTheme(1);
                  print("点击了暗色");
                },
                child: Text('暗色')),
            TextButton(
                onPressed: () {
                  Get.find<ThemeController>().setTheme(2);
                  // ProvideConfig.value<ThemeConfig>(context).setTheme(2);
                  print("点击了紫色");
                },
                child: Text('紫色')),
          ],
        ),
      ),
    );
  }
}
