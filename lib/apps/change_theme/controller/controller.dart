/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-30 11:18:55
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-30 11:30:26
 */
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ThemeController extends GetxController {
  // 亮色主题，应用程序默认主题
  final ThemeData _lightTheme = ThemeData(
    // 亮色
    brightness: Brightness.light,
    // 主背景色
    primaryColor: Colors.blue,
  );

  // 暗色主题，应用程序默认主题
  final ThemeData _darkTheme = ThemeData(
    // 暗色
    brightness: Brightness.dark,
    // 主背景色
    primaryColor: Colors.grey,
  );
  // 紫色主题
  final ThemeData _violetTheme = ThemeData(
    // 亮色
    brightness: Brightness.light,
    // 主背景色
    primaryColor: Colors.deepPurple,
  );

  // 程序已进入的默认主题，应用程序默认主题
  ThemeData _defaultTheme = ThemeData(
    // 亮色
    brightness: Brightness.light,
    // 主背景色
    primaryColor: Colors.blue,
  );

  // 对完开放的数据
  ThemeData get defaultTheme => _defaultTheme;
  void setTheme(num index) {
    switch (index) {
      case 0:
        _defaultTheme = _lightTheme;
        break;
      case 1:
        _defaultTheme = _darkTheme;
        break;
      case 2:
        _defaultTheme = _violetTheme;
        break;
      default:
    }
    // 改变主题
    Get.changeTheme(_defaultTheme);
    update();
  }
}
