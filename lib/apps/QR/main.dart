import 'package:flutter/material.dart';

import 'screens/components/button_widget.dart';
import 'screens/create/create_screen.dart';
import 'screens/open/open_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Flutter 二维码",
        theme: ThemeData(
          primaryColor: Colors.red,
          scaffoldBackgroundColor: Colors.black,
        ),
        home: MainPage(),
      );
}

class MainPage extends StatelessWidget {
  const MainPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("Flutter 二维码开发"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ButtonWidget(
                text: '生成二维码',
                onClicked: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => CreateScreen(),
                )),
              ),
              const SizedBox(height: 32),
              ButtonWidget(
                text: '扫描二维码',
                onClicked: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => OpenScreen(),
                )),
              ),
            ],
          ),
        ),
      );
}
