/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-27 06:58:22
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-29 17:32:20
 */
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_faster_study/apps/QR/screens/components/button_widget.dart';

class OpenScreen extends StatefulWidget {
  const OpenScreen({Key key}) : super(key: key);

  @override
  _OpenScreenState createState() => _OpenScreenState();
}

class _OpenScreenState extends State<OpenScreen> {
  String qrCode = "未知";
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("查看二维码"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '扫描结果',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.white54,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                qrCode,
                style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 72),
              ButtonWidget(
                text: '开始扫描',
                onClicked: () => scanQRCode(),
              ),
            ],
          ),
        ),
      );

  Future<void> scanQRCode() async {
    // try {
    //   final qrCode = await FlutterBarcodeScanner.scanBarcode(
    //     '#ff6666',
    //     '取消',
    //     true,
    //     ScanMode.QR,
    //   );
    //   if (!mounted) return; // 未加载成功，直接返回
    //   setState(() {
    //     this.qrCode = qrCode;
    //   });
    // } on PlatformException {
    //   qrCode = '获取信息失败';
    // }
  }
}
