import 'package:barcode_widget/barcode_widget.dart';
import 'package:flutter/material.dart';

class CreateScreen extends StatefulWidget {
  const CreateScreen({Key key}) : super(key: key);

  @override
  _CreateScreenState createState() => _CreateScreenState();
}

class _CreateScreenState extends State<CreateScreen> {
  final controller = TextEditingController();
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("生成二维码"),
        ),
        body: Center(
          child: SingleChildScrollView(
            padding: EdgeInsets.all(24),
            child: Column(
              children: [
                BarcodeWidget(
                  data: controller.text,
                  barcode: Barcode.qrCode(),
                  color: Colors.white,
                  width: 200,
                  height: 200,
                ),
                SizedBox(height: 40),
                Row(
                  children: [
                    Expanded(
                      child: buildTextFiled(context),
                    ),
                    const SizedBox(width: 12),
                    FloatingActionButton(
                        backgroundColor: Theme.of(context).primaryColor,
                        child: Icon(Icons.done, size: 30),
                        onPressed: () => setState(() {})),
                  ],
                ),
              ],
            ),
          ),
        ),
      );

  buildTextFiled(BuildContext context) => TextField(
        controller: controller,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: "输入内容",
          hintStyle: TextStyle(
            color: Colors.grey,
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(16),
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(16),
            borderSide: BorderSide(
              color: Theme.of(context).primaryColor,
            ),
          ),
        ),
      );
}
