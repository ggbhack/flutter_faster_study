import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  const ButtonWidget({Key key, this.text, this.onClicked}) : super(key: key);

  final String text;
  final VoidCallback onClicked;

  @override
  Widget build(BuildContext context) => ElevatedButton(
        onPressed: onClicked,
        child: Text(
          text,
          style: TextStyle(
            fontSize: 24,
          ),
        ),
        style: TextButton.styleFrom(
          shape: StadiumBorder(),
          padding: EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 12,
          ),
        ),
      );
}
