/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 17:01:52
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 17:19:43
 */
import 'dart:async';

import 'package:battery/battery.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final battery = Battery();
  int batteryLevel = 100;
  Timer timer;
  BatteryState batteryState = BatteryState.full;
  StreamSubscription<BatteryState> subscription;

  @override
  void initState() {
    super.initState();
    listenBatteryLevel();
    listenBatteryState();
  }

  void listenBatteryState() {
    subscription = battery.onBatteryStateChanged.listen((BatteryState state) {
      setState(() {
        batteryState = state;
      });
    });
  }

  void listenBatteryLevel() {
    updateBatterylevel();
    timer = Timer.periodic(
      Duration(seconds: 10),
      (_) async => updateBatterylevel(),
    );
  }

  Future updateBatterylevel() async {
    final batteryLevel = await battery.batteryLevel;
    setState(() {
      this.batteryLevel = batteryLevel;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    timer.cancel();
    subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("监听充电状态")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            buildBatteryState(batteryState),
            const SizedBox(height: 32),
            buildBatteryLevel(batteryLevel),
          ],
        ),
      ),
    );
  }

  Widget buildBatteryLevel(int batteryLevel) => Text(
        '$batteryLevel%',
        style: TextStyle(
          fontSize: 32,
          color: Colors.green,
          fontWeight: FontWeight.bold,
        ),
      );

  Widget buildBatteryState(BatteryState batteryState) {
    final style = TextStyle(fontSize: 32, color: Colors.white);
    final double size = 300;
    switch (batteryState) {
      case BatteryState.full:
        final color = Colors.green;
        return Column(
          children: [
            Icon(LineIcons.batteryFull, size: size, color: color),
            Text('能量满满!', style: style.copyWith(color: color)),
          ],
        );
      case BatteryState.charging:
        final color = Colors.green;
        return Column(
          children: [
            Icon(LineIcons.battery34Full, size: size, color: color),
            Text('补充能量...', style: style.copyWith(color: color)),
          ],
        );

      case BatteryState.discharging:
      default:
        final color = Colors.red;
        return Column(
          children: [
            Icon(LineIcons.battery34Full, size: size, color: color),
            Text('能量掉线了...', style: style.copyWith(color: color)),
          ],
        );
    }
  }
}
