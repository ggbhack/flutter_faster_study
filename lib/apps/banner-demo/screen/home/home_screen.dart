import 'package:flutter/material.dart';

import 'components/banner_base_page.dart';
import 'components/banner_custom_page.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: buildBottomBar(),
      body: buildPages(),
    );
  }

  Widget buildBottomBar() {
    final style = TextStyle(color: Colors.black);
    return BottomNavigationBar(
      backgroundColor: Theme.of(context).primaryColor,
      selectedItemColor: Colors.black,
      unselectedItemColor: Colors.black54,
      currentIndex: index,
      items: [
        BottomNavigationBarItem(icon: Container(), label: '标签'),
        BottomNavigationBarItem(icon: Container(), label: '自定义标签'),
      ],
      onTap: (int index) => setState(() {
        this.index = index;
      }),
    );
  }

  Widget buildPages() {
    switch (index) {
      case 0:
        return BannerBasePage();
      case 1:
        return BannerCustomPage();
      default:
        return Container();
    }
  }
}
