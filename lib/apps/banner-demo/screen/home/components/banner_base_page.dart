import 'package:flutter/material.dart';

import 'card_widget.dart';

class BannerBasePage extends StatelessWidget {
  const BannerBasePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("基础的banner"),
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.all(16),
        children: [
          buildBanner(location: BannerLocation.topStart),
          SizedBox(height: 16),
          buildBanner(location: BannerLocation.topEnd),
          SizedBox(height: 16),
          buildBanner(location: BannerLocation.bottomStart),
          SizedBox(height: 16),
          buildBanner(location: BannerLocation.bottomEnd),
        ],
      ),
    );
  }

  buildBanner({BannerLocation location}) => ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Banner(message: "推荐", location: location, child: CardWidget()),
      );
}
