import 'package:flutter/material.dart';

import 'card_widget.dart';

class BannerCustomPage extends StatelessWidget {
  final topLeft = AlwaysStoppedAnimation(-45 / 360);
  final topRight = AlwaysStoppedAnimation(45 / 360);
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: Text("自定义banner"),
      ),
      body: ListView(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.all(16),
          children: [
            buildCardTopLeft(),
            SizedBox(height: 16),
            buildCardTopRight(),
            SizedBox(height: 16),
          ]));

  buildCardTopLeft() => Stack(children: [
        CardWidget(),
        Positioned(
          left: -32,
          top: 20,
          child: buildBadge(turns: topLeft),
        ),
      ]);

  buildCardTopRight() => Stack(children: [
        CardWidget(),
        Positioned(
          right: -32,
          top: 20,
          child: buildBadge(turns: topRight),
        ),
      ]);

  buildBadge({Animation<double> turns}) => RotationTransition(
        turns: turns,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 36),
          color: Colors.teal,
          child: Text(
            "热门产品",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontSize: 20,
            ),
          ),
        ),
      );
}
