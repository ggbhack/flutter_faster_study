/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 15:34:34
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 15:35:58
 */
class CardModel {
  final String title;
  final String desc;
  final String cover;
  final String content;
  CardModel({this.title, this.desc, this.cover, this.content});
}
