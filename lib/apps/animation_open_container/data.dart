/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 15:34:23
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 15:46:32
 */
import 'model/card_model.dart';

List<CardModel> cardList = [
  new CardModel(
    title: '标题1',
    desc: '我是描述',
    cover: 'assets/images/icon_1.png',
    content: 'content',
  ),
  new CardModel(
    title: '标题2',
    desc: '我是描述',
    cover: 'assets/images/icon_2.png',
    content: 'content',
  ),
  new CardModel(
    title: '标题3',
    desc: '我是描述',
    cover: 'assets/images/icon_3.png',
    content: 'content',
  ),
  new CardModel(
    title: '标题4',
    desc: '我是描述',
    cover: 'assets/images/icon_4.png',
    content: 'content',
  ),
  new CardModel(
    title: '标题5',
    desc: '我是描述',
    cover: 'assets/images/icon_5.png',
    content: 'content',
  ),
  new CardModel(
    title: '标题6',
    desc: '我是描述',
    cover: 'assets/images/icon_6.png',
    content: 'content',
  ),
  new CardModel(
    title: '标题7',
    desc: '我是描述',
    cover: 'assets/images/icon_7.png',
    content: 'content',
  ),
];
