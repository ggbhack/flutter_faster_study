/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 15:22:43
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 16:02:09
 */
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/animation_open_container/screens/detail/detail_screen.dart';
import 'package:flutter_faster_study/apps/animation_open_container/screens/home/components/card_widget.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../../data.dart';
import 'components/custom_fab_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final transitionType = ContainerTransitionType.fade;
  final items = List.from(cardList);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("插画长廊"),
      ),
      body: StaggeredGridView.countBuilder(
        crossAxisCount: 4,
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0,
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) => OpenContainer(
          transitionType: transitionType,
          transitionDuration: Duration(milliseconds: 500),
          openBuilder: (context, _) => DetailScreen(card: items[index]),
          closedBuilder: (context, VoidCallback openContainer) => CardWidget(
            card: items[index],
            onClicked: openContainer,
          ),
        ),
        staggeredTileBuilder: (int index) =>
            new StaggeredTile.count(2, index.isEven ? 3 : 2),
      ),
      floatingActionButton: CustomFABWidget(),
    );
  }
}
