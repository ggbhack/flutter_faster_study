/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 16:13:30
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 16:40:40
 */
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/animation_open_container/screens/detail/detail_screen.dart';

import '../../../data.dart';

class CustomFABWidget extends StatelessWidget {
  const CustomFABWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => OpenContainer(
        openBuilder: (context, _) => DetailScreen(card: cardList.first),
        closedShape: CircleBorder(),
        closedColor: Theme.of(context).primaryColor,
        transitionDuration: Duration(milliseconds: 500),
        closedBuilder: (context, openContainer) => Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Theme.of(context).primaryColor,
          ),
          height: 56,
          width: 56,
          child: Icon(
            Icons.remove_red_eye,
            color: Colors.black,
          ),
        ),
      );
}
