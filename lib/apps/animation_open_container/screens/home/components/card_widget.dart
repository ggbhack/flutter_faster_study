/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 15:33:46
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 15:59:54
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/animation_open_container/model/card_model.dart';

class CardWidget extends StatelessWidget {
  const CardWidget({Key key, this.card, this.onClicked}) : super(key: key);
  final CardModel card;
  final VoidCallback onClicked;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey,
      ),
      child: GestureDetector(
        onTap: onClicked,
        child: Column(
          children: [
            Image.asset(card.cover),
            Text(card.title),
            Text(card.desc),
          ],
        ),
      ),
    );
  }
}
