/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-08-04 15:47:38
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-04 15:57:42
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/animation_open_container/model/card_model.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key key, this.card}) : super(key: key);
  final CardModel card;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(card.title),
      ),
      body: Container(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                card.desc,
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                card.content,
              ),
              Image.asset(card.cover),
              Image.network(
                  "https://tse1-mm.cn.bing.net/th/id/R-C.3140c20aadff937e10f24f5d90b1ce54?rik=0tAXkgIzPJxkaQ&riu=http%3a%2f%2fwww.uuudoc.com%2fimg%2f2W3838341M1B1B2X312P2V2T1D1C1L1A1F1I1C2S332R1A2R33311B1W333B3230332P2S21312V1B1E1C1D1L1B1C1G1B1D1F1D1J1B1D1H1K1J1C1K1E1L1L2N1D1E2N1E1C1D1L1C1G1D1F1C1H1D1E1D1J1K1D1H.jpg&ehk=QjeFDaRDWx5NS%2bQ959PAW4RFB7UCzOgR6UnJH5w9s2s%3d&risl=&pid=ImgRaw&r=0"),
            ],
          ),
        ),
      ),
    );
  }
}
