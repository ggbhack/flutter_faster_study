import 'package:flutter/material.dart';
import 'package:flutter_faster_study/apps/QR/screens/components/button_widget.dart';
import 'package:animations/animations.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      value: 0,
      duration: Duration(seconds: 2),
      reverseDuration: Duration(seconds: 2),
      vsync: this,
    )..addStatusListener((status) => setState(() {}));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }

  // 判断当前动画是否执行完成
  bool get isFormwardAnimation =>
      controller.status == AnimationStatus.forward ||
      controller.status == AnimationStatus.completed;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("渐变动画控制"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ButtonWidget(
                text: "渐变动画Dialog",
                onClicked: () => showCustomeDialog(context),
              ),
              SizedBox(height: 24),
              ButtonWidget(
                text: isFormwardAnimation ? '隐藏悬浮按钮' : '显示悬浮按钮',
                onClicked: toggleFAB,
              ),
            ],
          ),
        ),
        floatingActionButton: buildFloatingButton(),
      );

  Future showCustomeDialog(BuildContext context) => showModal(
        configuration: FadeScaleTransitionConfiguration(
          transitionDuration: Duration(seconds: 2),
          reverseTransitionDuration: Duration(seconds: 2),
        ),
        context: context,
        builder: (context) => AlertDialog(
          content: Text("渐变动画Dialog"),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text("取消"),
            ),
          ],
        ),
      );

  Future toggleFAB() =>
      isFormwardAnimation ? controller.reverse() : controller.forward();
  Widget buildFloatingButton() => AnimatedBuilder(
        animation: controller,
        builder: (context, child) => FadeScaleTransition(
          animation: controller,
          child: child,
        ),
        child: Visibility(
          visible: controller.status != AnimationStatus.dismissed,
          child: FloatingActionButton(child: Icon(Icons.add), onPressed: () {}),
        ),
      );
}
