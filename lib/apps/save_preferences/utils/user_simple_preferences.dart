/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-30 15:35:22
 * @LastEditors: GGB
 * @LastEditTime: 2021-08-02 17:07:32
 */
import 'package:shared_preferences/shared_preferences.dart';

class UserSimplePreferences {
  static const _keyUsername = "username";
  static const _keyPets = "pets";
  static const _keyBirthday = "birthday";
  // 初始化
  static SharedPreferences _preferences;
  static Future init() async =>
      _preferences = await SharedPreferences.getInstance();
// 设置和获取姓名
  static Future setUsername(String username) async =>
      await _preferences.setString(_keyUsername, username);
  static String getUsername() => _preferences.getString(_keyUsername) ?? '';
// 设置和获取宠物
  static Future setPets(List<String> pets) async =>
      await _preferences.setStringList(_keyPets, pets);
  static List<String> getPets() => _preferences.getStringList(_keyPets);

// 设置和获取生日
  static Future setBirthday(DateTime dateOfBirth) async {
    final birthday = dateOfBirth.toIso8601String();
    return await _preferences.setString(_keyBirthday, birthday);
  }

  static DateTime getBirthday() {
    final birthday = _preferences.getString(_keyBirthday);
    return birthday == null ? null : DateTime.tryParse(birthday);
  }
}
