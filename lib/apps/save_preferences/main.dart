/*
 * @Descripttion: GGB
 * @version: 1.0
 * @Author: GGB
 * @Date: 2021-07-30 15:32:55
 * @LastEditors: GGB
 * @LastEditTime: 2021-07-30 17:19:14
 */

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_localizations/flutter_localizations.dart';

import 'screens/home/home_screen.dart';
import 'utils/user_simple_preferences.dart';

Future main() async {
  // 初始化
  WidgetsFlutterBinding.ensureInitialized();
  // 设置横竖屏 锁定方向
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  await UserSimplePreferences.init();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // localizationsDelegates: [
      //   //去提供应用里面的本地化组件
      //   GlobalMaterialLocalizations.delegate,
      //   //小部件默认的方向 根据文字切换左右方向
      //   GlobalWidgetsLocalizations.delegate,
      // ],
      // supportedLocales: [
      //   Locale('en', "US"),
      //   Locale('zh', "CN"),
      // ],
      title: '偏好设置',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.dark(),
        scaffoldBackgroundColor: Color(0xff2e5967),
        accentColor: Color(0xff2e5967).withGreen(150),
        unselectedWidgetColor: Color(0xff2e5967).withRed(200),
      ),
      home: HomeScreen(),
    );
  }
}
