/*
 * @Author: your name
 * @Date: 2021-07-26 13:38:44
 * @LastEditTime: 2021-07-26 17:39:28
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\components\glass_widget.dart
 */
import 'package:flutter/material.dart';
import 'package:glassmorphism/glassmorphism.dart';

class GlassWidget extends StatelessWidget {
  const GlassWidget({
    Key key,
    this.width,
    this.height,
    this.borderRadius,
    this.blur,
    this.linearGradientBeginOpacity,
    this.linearGradientEndOpacity,
    this.borderGradientBeginOpacity,
    this.borderGradientEndOpacity,
    this.child,
    this.bgColor,
  }) : super(key: key);
  final double width;
  final double height;
  final Color bgColor;
  final double borderRadius;
  final double blur;
  final double linearGradientBeginOpacity;
  final double linearGradientEndOpacity;
  final double borderGradientBeginOpacity;
  final double borderGradientEndOpacity;
  final Widget child;
  @override
  Widget build(BuildContext context) {
    Color bg = bgColor != null ? bgColor : Color(0xFFFFFFFF);
    return Container(
      child: GlassmorphicContainer(
        width: width,
        height: height,
        borderRadius: borderRadius,
        blur: blur,
        alignment: Alignment.bottomCenter,
        border: 2,
        linearGradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            bg.withOpacity(linearGradientBeginOpacity),
            bg.withOpacity(linearGradientEndOpacity),
          ],
          stops: [0.1, 1],
        ),
        borderGradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            bg.withOpacity(borderGradientBeginOpacity),
            bg.withOpacity(borderGradientEndOpacity),
          ],
        ),
        child: child,
      ),
    );
  }
}
