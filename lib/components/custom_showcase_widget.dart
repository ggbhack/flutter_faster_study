import 'package:flutter/material.dart';
import 'package:showcaseview/showcaseview.dart';

class CustomShowcaseWidget extends StatelessWidget {
  const CustomShowcaseWidget(
      {Key key,
      @required this.child,
      @required this.desctiption,
      @required this.globalKey})
      : super(key: key);
  final Widget child;
  final String desctiption;
  final GlobalKey globalKey;
  @override
  Widget build(BuildContext context) => Showcase.withWidget(
        overlayColor: Colors.black,
        overlayOpacity: 0.3,
        height: 80,
        width: 140,
        shapeBorder: CircleBorder(),
        key: globalKey,
        description: desctiption,
        container: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Color(0xfff2abc3),
                borderRadius: BorderRadius.circular(10),
              ),
              padding: EdgeInsets.all(10),
              child: Text(
                "$desctiption",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
        child: child,
      );
}
