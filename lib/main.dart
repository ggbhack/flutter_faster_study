/*
 * @Author: your name
 * @Date: 2021-07-25 20:15:48
 * @LastEditTime: 2021-07-26 19:19:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\main.dart
 */
import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';

import 'screens/home/home_screen.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

// Future main() async {
//   // 初始化
//   WidgetsFlutterBinding.ensureInitialized();
//   // 设置横竖屏 锁定方向
//   await SystemChrome.setPreferredOrientations([
//     DeviceOrientation.portraitUp,
//     DeviceOrientation.portraitDown,
//   ]);

//   runApp(MyApp());
// }

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: '萃元小学家长开放日',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
    );
  }
}
