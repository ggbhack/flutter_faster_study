/*
 * @Author: your name
 * @Date: 2021-07-27 09:53:54
 * @LastEditTime: 2021-07-28 14:45:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\model\menu.dart
 */
class MenuModel {
  String label;
  String value;
  MenuModel(this.label, this.value);
}
