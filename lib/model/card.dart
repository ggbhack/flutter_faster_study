/*
 * @Author: your name
 * @Date: 2021-07-26 14:33:31
 * @LastEditTime: 2021-07-26 16:17:32
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\model\card.dart
 */
class CardModel {
  int allNum;
  CardModel({this.allNum});
  factory CardModel.fromJson(dynamic json) {
    return CardModel(allNum: json['Card']);
  }
}
