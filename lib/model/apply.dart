/*
 * @Author: your name
 * @Date: 2021-07-26 14:33:42
 * @LastEditTime: 2021-07-28 11:13:53
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\model\apply.dart
 */
class ApplyModel {
  String createTime;
  String avatarUrl;
  String openId;
  int hasChild;
  String name;
  String updateTime;
  int id;
  int hasParent;
  int isReceive;

  ApplyModel({
    this.createTime,
    this.avatarUrl,
    this.openId,
    this.hasChild,
    this.name,
    this.updateTime,
    this.id,
    this.hasParent,
    this.isReceive,
  });

  factory ApplyModel.fromJson(dynamic json) {
    return ApplyModel(
      createTime: json['createTime'],
      avatarUrl: json['avatarUrl'],
      openId: json['openId'],
      hasChild: json['hasChild'],
      name: json['name'],
      updateTime: json['updateTime'],
      id: json['id'],
      hasParent: json['hasParent'],
      isReceive: json['isReceive'],
    );
  }
}

// 产品列表数据转换
class ApplyListModel {
  List<ApplyModel> data;
  ApplyListModel(this.data);
  factory ApplyListModel.fromJson(List json) {
    return ApplyListModel(json.map((i) => ApplyModel.fromJson((i))).toList());
  }
}
