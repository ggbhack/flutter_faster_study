/*
 * @Author: your name
 * @Date: 2021-07-26 14:16:03
 * @LastEditTime: 2021-07-28 15:12:12
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\screens\home\components\user_item.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/model/apply.dart';
import 'package:flutter_faster_study/controller/controller.dart';
import 'package:flutter_faster_study/services/apply.dart';

import '../../../constants.dart';
import 'package:get/get.dart';

// import 'dart:convert';

class UserItem extends StatelessWidget {
  const UserItem({
    Key key,
    this.apply,
  }) : super(key: key);

  final ApplyModel apply;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: defaultPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // CircleAvatar(
          //   radius: 20,
          //   backgroundImage: Image.network(src),
          // ),
          apply.avatarUrl != null
              ? Image.network(
                  apply.avatarUrl,
                  width: 40,
                )
              : Image.asset(
                  "assets/images/icon.png",
                  width: 40,
                ),
          SizedBox(width: defaultPadding),
          Expanded(
            child: Text(
              apply.name,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: defaultFontSize,
                color: Colors.white,
              ),
            ),
          ),
          ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(
                apply.isReceive != 1
                    ? Theme.of(context).primaryColor
                    : Colors.grey,
              ),
            ),
            onPressed: () {
              if (apply.isReceive == 1) return;
              showCupertinoDialog(
                  context: context,
                  builder: (context) {
                    return CupertinoAlertDialog(
                      title: Text("提示"),
                      content: Text("是否确定发放礼物"),
                      actions: [
                        TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            "取消",
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ),
                        TextButton(
                          onPressed: () async {
                            // Get.find<ApplyController>().sendGift(apply);
                            var data = await updateGiftStatus(apply);
                            if (data != null) {
                              Navigator.of(context).pop();
                              // ToastUtils.showToast("已发放");
                              Get.find<ApplyController>().requestApply();
                            }
                          },
                          child: Text("确认"),
                        ),
                      ],
                    );
                  });
            },
            child: Text(apply.isReceive == 1 ? "已领取" : "未领取"),
          ),
        ],
      ),
    );
  }
}
