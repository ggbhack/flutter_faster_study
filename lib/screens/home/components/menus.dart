/*
 * @Author: your name
 * @Date: 2021-07-27 09:40:49
 * @LastEditTime: 2021-07-28 10:53:47
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\screens\home\components\menus.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/controller/controller.dart';
import 'package:get/get.dart';

class Menus extends StatefulWidget {
  const Menus({Key key}) : super(key: key);

  @override
  _MenusState createState() => _MenusState();
}

class _MenusState extends State<Menus> {
  ApplyController _applyController = Get.put(ApplyController());

  @override
  Widget build(BuildContext context) => GetBuilder<ApplyController>(
      builder: (_controller) => PopupMenuButton<String>(
            onSelected: (value) {
              _applyController.setType(value);
            },
            icon: Icon(
              Icons.more_vert,
              color: Colors.white,
            ),
            padding: EdgeInsets.all(6),
            offset: Offset(0, 30),
            itemBuilder: (context) {
              return <PopupMenuEntry<String>>[
                ..._controller.menus
                    .map((menu) => PopupMenuItem<String>(
                          value: "${menu.value}",
                          child: Text(
                            menu.label,
                            style: TextStyle(
                              color: menu.value == _controller.type
                                  ? Theme.of(context).primaryColor
                                  : Colors.grey,
                            ),
                          ),
                        ))
                    .toList()
              ];

              // [
              //   CheckedPopupMenuItem<String>(
              //     value: 'all',
              //     checked:
              //     child: Text('全部'),
              //   ),
              //   PopupMenuDivider(),
              //   CheckedPopupMenuItem<String>(
              //     value: 'yes',
              //     checked:
              //     child: Text('已领取'),
              //   ),
              //   PopupMenuDivider(),
              //   CheckedPopupMenuItem<String>(
              //     value: 'no',
              //     checked:
              //     child: Text('未领取'),
              //   ),
              // ];
            },
          ));
}
