/*
 * @Author: your name
 * @Date: 2021-07-26 14:14:05
 * @LastEditTime: 2021-07-27 11:50:55
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\screens\home\components\count_card.dart
 */
import 'package:flutter/material.dart';

import '../../../constants.dart';

class CountCard extends StatelessWidget {
  const CountCard({Key key, this.color, this.count, this.label})
      : super(key: key);

  final Color color;
  final int count;
  final String label;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 10,
        ),
        decoration: BoxDecoration(
          color: color.withOpacity(0.7),
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Column(
          children: [
            Text(
              "$count",
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
            Text(
              label,
              style: TextStyle(
                fontSize: defaultFontSize,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
