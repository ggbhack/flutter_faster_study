/*
 * @Author: your name
 * @Date: 2021-07-26 07:04:25
 * @LastEditTime: 2021-07-28 11:15:10
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \flutter_faster_study\lib\apps\ui_code_clean\screens\home\home_screen.dart
 */
import 'package:flutter/material.dart';
import 'package:flutter_faster_study/components/glass_widget.dart';
import 'package:flutter_faster_study/controller/controller.dart';
// import 'package:flutter_faster_study/model/apply.dart';

import 'package:flutter_faster_study/screens/home/components/count_card.dart';
// import 'package:flutter_faster_study/services/apply.dart';
// import 'package:flutter_faster_study/services/card.dart';
import 'package:get/get.dart';

import '../../constants.dart';
import 'components/menus.dart';
import 'components/user_item.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // ApplyListModel listData = ApplyListModel.fromJson([]);
  // getX注入

  ApplyController _applyController = Get.put(ApplyController());
  // int applyCount = 0;
  // int viewCount = 0;

  // void getApplyList() async {
  //   var data = await getApply();
  //   var applyData = await getApplyCount();
  //   var viewData = await getViewCount();
  //   ApplyListModel list = ApplyListModel.fromJson(data);
  //   // CardModel applyCount = CardModel.fromJson(applyData).allNum;
  //   setState(() {
  //     // 一次性将所获取的内容全部填充listData中
  //     listData.data.addAll(list.data);
  //     applyCount = applyData['allNum'];
  //     viewCount = viewData['allNum'];
  //   });
  // }

  @override
  void initState() {
    super.initState();
    _applyController.requestApply();
    _applyController.requestApplyCount();
    _applyController.requestViewCount();
    // getApplyList();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Stack(
          children: [
            Image.asset(
              'assets/images/bg.jpg',
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
              scale: 1,
            ),
            SafeArea(
              child: Center(
                child: GlassWidget(
                  width: width * 0.9,
                  height: height,
                  borderRadius: 30,
                  blur: 20,
                  linearGradientBeginOpacity: 0.3,
                  linearGradientEndOpacity: 0.2,
                  borderGradientBeginOpacity: 0.5,
                  borderGradientEndOpacity: 0.02,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: GetBuilder<ApplyController>(
                      builder: (_controller) => Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "萃元小学家长开放日",
                            style:
                                Theme.of(context).textTheme.headline5.copyWith(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                    ),
                          ),
                          SizedBox(
                            height: defaultPadding,
                          ),
                          Row(
                            children: [
                              CountCard(
                                count: _controller.applyCount,
                                label: "预约人数",
                                color: Color(0xff4E7CF3),
                              ),
                              SizedBox(width: 16),
                              CountCard(
                                count: _controller.viewCount,
                                label: "浏览量",
                                color: Color(0xffFF7324),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: defaultPadding / 2,
                          ),
                          Expanded(
                              child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "预约列表",
                                      style: TextStyle(
                                        fontSize: defaultFontSize,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          _controller.getSelected(),
                                          style: TextStyle(
                                            fontSize: defaultFontSize,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        Menus(),
                                      ],
                                    ),
                                  ],
                                ),
                                Divider(color: Colors.white),
                                // 构建列表
                                ...List.generate(
                                  _controller.applyList.length,
                                  (index) => UserItem(
                                    apply: _controller.applyList[index],
                                  ),
                                ).toList()
                              ],
                            ),
                          )),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
